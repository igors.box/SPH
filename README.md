# SPH

This is [SPH (Smoothed Particle Hydrodynamics)](https://en.wikipedia.org/wiki/SPH) realization.

It is written using Fortran with MPI cluster support.

Written as a part of post graduate work by Igor Yakovlev Saint-Petersburg State University.

Please feel free to mail me: [igors.box@mail.ru](igors.box@mail.ru) if any question accurs.

### Build
Run **make** to build SPH with mpif90

Run **make COMPILE_WITH=intel** to build SPH with mpiifort

or with MS Visual Studio with Intel Parallel Studio installed.