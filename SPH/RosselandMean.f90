module RosselandMean
    use :: Parameters
    implicit none
        real(8), parameter :: nu_min = c_const / 1d5 * R_pl * sqrt(rho_out / p_out)  ! 1 км
        real(8), parameter :: nu_max = c_const / 2d-6 * R_pl * sqrt(rho_out / p_out) ! 200 ангстрем
        real(8), parameter :: integralStepsCount = 1d3
    
        real(8), parameter :: Z = 1d0
        real(8), parameter :: mainCoefficient1 = 1.67880d-64 !(1d0) / ((64d0) * ((pi_const)**4) / 3d0 / sqrt((3d0)) / ((h_const)**6) * (m_e_const) * ((E_e_const)**10) * (Z**4) / (c_const)) * (m_H_const)
        real(8), parameter :: mainCoefficient2 = (15d0) / (4d0) / ((pi_const)**4)
        real(8), parameter :: mainCoefficient3 = ((p_out)**1.5d0) / ((R_pl)**4) / ((rho_out)**2.5d0)  !нормировка
        real(8), parameter :: mainCoefficient = mainCoefficient1 * mainCoefficient2 !* mainCoefficient3
        integer(4), parameter :: n_max = 100 !максимальный уровень ионизации
    contains

    function GetAbsorbtion(T, nu, n_min) result(absorbtion)
        real(8), intent(in) :: T, nu
        integer(4), intent(in) :: n_min
        integer(4) :: iter
        real(8) :: absorbtion
        real(8) :: x_1

        real(8), save :: savedT 
        real(8), save :: savedRrakingRadiation 
        real(8), dimension(n_max), save :: savedAbsorbtionSums
        logical, save :: firstCall = .true.
    
        !кэширование вызова для Rho и T
        if(firstCall .or. savedT /= T) then
            x_1 = E1_H_const1 / k_B_const1 / T ! энергия ионизации с основного уровня водорода

            !посчитаем тормозное излучение
            savedRrakingRadiation = exp(-x_1) / 2d0 / x_1
            
            !посчитаем все поглощения для всех уровней
            do iter = 1, n_max
                savedAbsorbtionSums(iter) = 1d0 / (real(iter)**3) * exp(x_1 * (1d0 / (real(iter)**2) - 1d0))
            enddo
            do iter = n_max-1, 1, -1
                savedAbsorbtionSums(iter) = savedAbsorbtionSums(iter) + savedAbsorbtionSums(iter+1)
            enddo
            savedAbsorbtionSums = savedAbsorbtionSums + savedRrakingRadiation

            savedRrakingRadiation = savedRrakingRadiation
            savedAbsorbtionSums = savedAbsorbtionSums
        
            firstCall = .false.
            savedT = T
        endif

        if(n_min <= n_max) then 
 	        absorbtion = savedAbsorbtionSums(n_min)
        else	
            absorbtion = savedRrakingRadiation
        endif

        absorbtion = absorbtion / (nu**3)
    
        !absorbtion = absorbtion * 64 * pow(pi_const, 4) / 3 / sqrt(3) / pow(h_const1, 6) * m_e_const1 * pow(E_e_const1, 10) * pow(Z, 4) / m_H_const1  / c_const1 / pow(nu, 3)
        !absorbtion = absorbtion * (h_const1 / k_B_const1 / T)**3  			! ChangeHere 
    end function
        
    function GetRosselandMean(rho, T, limit, limited) result(l)
        real(8), intent(in) :: rho, T, limit
        real(8) :: default_d_nu, nu, nextPartialNu, x, l_nu, nu_H1, d_nu, f_a, f_b
        real(8) :: l, l_limit
        integer(4) :: currentN, nextN
        logical, intent(out) :: limited
        
        if(T == 0 .or. rho == 0) then
            write(*,*) "Rosseland for T=0 or rho=0"
        endif

        nu_H1 = E1_H_const1 / h_const1  ! частота ионизации с основного уровня

        default_d_nu = (nu_max - nu_min) / integralStepsCount
        nextN = n_max + 1
        currentN = nextN
        nextPartialNu = nu_H1 / (n_max**2)
        nu = nu_min
        l = 0d0

        l_limit = limit / h_const1 * k_B_const1 * T
        l_limit = l_limit * rho
        l_limit = l_limit / mainCoefficient
        
        f_a = f()
        
        if(f_a > l_limit) then
            l = limit
            limited = .true.
            return
        endif
    
        do while (nu < nu_max)
        
            currentN = nextN
            d_nu = default_d_nu
            nu = nu + default_d_nu

            if (nu > nextPartialNu .and. currentN /= 1) then
                d_nu = nextPartialNu - (nu - d_nu) !состояние до перепрыга
                nu = nextPartialNu !откатываем nu
                nextN = currentN - 1
                if(nextN /= 0) then
                    nextPartialNu = nu_H1 / (nextN**2)
                endif
            endif
            
            f_b = f()
        
            l = l + d_nu * (f_a + f_b) / 2d0

            if(l > l_limit) then
                l = limit
                limited = .true.
                return
            endif
            
            f_a = f_b
            if(currentN == 1) then  
                default_d_nu = default_d_nu * 1.2d0
            endif
        enddo
        l = l * h_const1 / k_B_const1 / T

        !l = l / ((64d0) * ((pi_const)**4) / 3 / sqrt((3d0)) / ((h_const1)**6) * (m_e_const1) * ((E_e_const1)**10) * (Z**4) / (c_const1)) / (m_H_const1)
        !l = l * (15d0) / (4d0) / ((pi_const)**4)
        !l = l * (h_const1) / (k_B_const1) !
    
        !N = rho / m_H_const1  ! концентрация водорода в данной области
        l = l / rho
        l = l * mainCoefficient
        limited = .false.
    
        contains
        function f() result(f_result)
            real(8) :: f_result
            real(8) :: absorbtion, exponent
            x = h_const1 * nu / k_B_const1 / T
            absorbtion = GetAbsorbtion(T, nu, currentN)
            exponent = (1d0 - exp(-x))**3
            if(absorbtion /= 0d0 .and. exponent /= 0d0) then
                l_nu =  1d0 / absorbtion
                f_result = l_nu * (x**4) * exp(-x) / exponent
            else
                write(*,*) "Invalid Rosseland values"
                l_nu = huge(l_nu)
                f_result = huge(f_result)
            endif
                
        end function
    end function


end module
