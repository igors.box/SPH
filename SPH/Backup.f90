#include "SPHConfig.f90"

module Backup
	use :: SPH
	use :: MpiHelpers
	use :: SPHArea
	implicit none

    contains

    subroutine BackupIteration(iterationNumber, fileCount)
        integer(4), intent(in) :: iterationNumber, fileCount
        integer(4) :: i, a
        integer(4), dimension(:), allocatable :: randomSeedStatus
        integer(4) :: randomSeedStatusSize
        character(2) :: rankchar
		
        if(MpiRank < 10) then
            write(rankchar,"(I1)") MpiRank
        else
            write(rankchar,"(I2)") MpiRank
        endif
        
        open(unit=1, file="backup/"//trim(adjustl(rankchar))//"/backup.data", status='replace', form='unformatted')

        write(1) interactParticles(0), dt1, dt, dNiterate, iterationNumber, fileCount
#ifdef DYNAMIC_DT
        write(1) dt1, dt, dNiterate
#endif
        !сохраняем Seed
        call random_seed(size = randomSeedStatusSize)
        allocate(randomSeedStatus(randomSeedStatusSize))
        call random_seed(get = randomSeedStatus)
        write(1) randomSeedStatusSize
        write(1) randomSeedStatus
        deallocate(randomSeedStatus)

        do i=1, interactParticles(0)
            a = interactParticles(i)
            write(1) point(a) % r(1), point(a) % r(2), point(a) % v(1), point(a) % v(2), point(a) % u, point(a) % ksi, point(a) % dr(1), point(a) % dr(2), point(a) % dv(1), point(a) % dv(2)
        enddo

        close(1)
    end subroutine
    
    subroutine RestoreIteration(iterationNumber, fileCount)
        integer(4), intent(out) :: iterationNumber, fileCount
        integer(4), dimension(:), allocatable :: randomSeedStatus
        integer(4) :: randomSeedStatusSize
        integer(4) :: i, a
        character(2) :: rankchar
        if(MpiRank < 10) then
            write(rankchar,"(I1)") MpiRank
        else
            write(rankchar,"(I2)") MpiRank
        endif
        
        open(unit=1, file="backup/"//trim(adjustl(rankchar))//"/backup.data", status='old', form='unformatted')
        
        read(1) interactParticles(0), dt1, dt, dNiterate, iterationNumber, fileCount
#ifdef DYNAMIC_DT
        read(1) dt1, dt, dNiterate
#endif
        !восстанавливаем Seed
        read(1) randomSeedStatusSize
        allocate(randomSeedStatus(randomSeedStatusSize))
        read(1) randomSeedStatus
        call random_seed(put = randomSeedStatus)
        deallocate(randomSeedStatus)
        
        do i=1, interactParticles(0)
            a = freePointPool % Acquire()
            interactParticles(i) = a
            
            read(1) point(a) % r(1), point(a) % r(2), point(a) % v(1), point(a) % v(2), point(a) % u, point(a) % ksi, point(a) % dr(1), point(a) % dr(2), point(a) % dv(1), point(a) % dv(2)
        enddo
        
        close(1)

        calculateParticles(0) = 0
        foreignPointsIndexes % Cursor = 0
        
        do i=1, interactParticles(0)
            a = interactParticles(i)

            if(IsCalculateParticle(point(a) % r(1), point(a) % r(2))) then
                calculateParticles(0) = calculateParticles(0) + 1
                calculateParticles(calculateParticles(0)) = a
            else
                call foreignPointsIndexes % SafeAddItem(a)
            endif

        enddo
        
#ifdef USE_CLUSTER_MODE
        call UpdateClusterGrid()
#endif

	end subroutine

end module
    