#include "SPHConfig.f90"

module MpiTransferPoint
    implicit none
    integer(4) :: MPI_TYPE_TransferPoint
    integer(4) :: MPI_TYPE_TransferPointRho
    integer(4) :: MPI_TYPE_TransferPointLambdaDivKappaRho
    integer(4) :: MPI_TYPE_TransferPointUNextKsiNext
    
    !ЛЮБОЕ ИЗМЕНЕНИЕ ЭТОЙ СТРУКТУРЫ НУЖДАЕТСЯ В ПРАВИЛЬНОМ ИЗМЕНЕНИИ КОДА В MpiRegisterTransferTypes
    type PointType
        sequence
        !MPI_TYPE_TransferPoint pos 0, size 6 * 8
        real(8), dimension(2) :: r
        real(8), dimension(2) :: v
        real(8) :: u
        real(8) :: ksi
        !end MPI_TYPE_TransferPoint
        
        !дополнительные данные для итерации
        !MPI_TYPE_TransferPointRho pos 6 * 8, size 1 * 8
        real(8) :: rho
        !end MPI_TYPE_TransferPointRho
        
        !дополнительные данные для итерации
        !MPI_TYPE_TransferPointLambdaDivKappaRho pos 7 * 8, size 1 * 8
        real(8) :: lambdaDivKappaRho !lambda_i / (k_i * point(i) % rho)
        !end MPI_TYPE_TransferPointLambdaDivKappaRho

        !данные для итерации неявного метода
        !MPI_TYPE_TransferPointUNextKsiNext pos 8 * 8, size 2 * 8
        real(8) :: uNext
        real(8) :: ksiNext
        !end MPI_TYPE_TransferPointUNextKsiNext
        
        !служебные поля (не учавствуют в пересылке)
        real(8) :: uNextNew
        real(8) :: ksiNextNew
        
        real(8), dimension(2) :: dr
        real(8) :: p
        real(8) :: T_gas
        real(8) :: T_rad
        real(8), dimension(2) :: dv
#ifdef DYNAMIC_DT
        real(8) :: divV
#endif
#ifdef CALCULATE_PARTICLE_INTERACTION_COUNT
        integer(8) :: interactionCount
#endif
        !коэффициенты из Whitehouse 2005
        logical(4) :: rosseland_was_cutted
        real(8) :: R_coefficient !Безразмерный коэфициент из Whitehouse 2005
        real(8) :: Kappa !Коффициент поглощения
        real(8) :: Lambda !Коэффициент отсечения
        real(8) :: a4
        real(8) :: a1
        real(8) :: hi
        real(8) :: beta
        real(8) :: Gamma
    endtype
    
    contains
    
    function RegisterPointStructureSlice(place, size, mpiType) result(mpiRegisteredType)
        use :: MpiHelpers
        integer(4), intent(in) :: place, size, mpiType
        integer(4) :: mpiRegisteredType
        integer(4) :: mpiError
        type(PointType) :: pointInstance
        integer(4) :: MPI_TYPE_Unsized
        integer(4), dimension(1) :: array_of_blocklengths, array_of_types
        integer(kind = mpi_address_kind), dimension(1) :: array_of_displacements
        integer(kind = mpi_address_kind) :: lb, extent

        array_of_displacements(1) = place
        array_of_blocklengths(1) = size
        array_of_types(1) = mpiType
        
        lb = 0
        extent = sizeof(pointInstance)

        call MPI_Type_create_struct(1, array_of_blocklengths, array_of_displacements, array_of_types, MPI_TYPE_Unsized, mpiError)
        call AssertMpiError(mpiError)
        call mpi_type_commit(MPI_TYPE_Unsized, mpiError)
        call AssertMpiError(mpiError)
        
        call MPI_Type_create_resized(MPI_TYPE_Unsized, lb, extent, mpiRegisteredType, mpiError)
        call AssertMpiError(mpiError)
        call mpi_type_commit(mpiRegisteredType, mpiError)
    end function
    
    subroutine MpiRegisterTransferTypes
        use :: MpiHelpers
        real(8) :: realInstance
        MPI_TYPE_TransferPoint = RegisterPointStructureSlice(0, 6, MPI_REAL8)
        MPI_TYPE_TransferPointRho = RegisterPointStructureSlice(int(6 * sizeof(realInstance)), 1, MPI_REAL8)
        MPI_TYPE_TransferPointLambdaDivKappaRho = RegisterPointStructureSlice(int(7 * sizeof(realInstance)), 1, MPI_REAL8)
        MPI_TYPE_TransferPointUNextKsiNext = RegisterPointStructureSlice(int(8 * sizeof(realInstance)), 2, MPI_REAL8)
    end subroutine
    
    subroutine MpiUnregisterTransferTypes()
        use :: MpiHelpers
        integer(4) :: mpiError
        !MPI_TYPE_TransferPoint
        call mpi_type_free(MPI_TYPE_TransferPoint, mpiError)
        call AssertMpiError(mpiError)
        !MPI_TYPE_TransferPointRho
        call mpi_type_free(MPI_TYPE_TransferPointRho, mpiError)
        call AssertMpiError(mpiError)
        !MPI_TYPE_TransferPointLambdaDivKappaRho
        call mpi_type_free(MPI_TYPE_TransferPointLambdaDivKappaRho, mpiError)
        call AssertMpiError(mpiError)
        !MPI_TYPE_TransferPointUNextKsiNext
        call mpi_type_free(MPI_TYPE_TransferPointUNextKsiNext, mpiError)
        call AssertMpiError(mpiError)
    end subroutine
end module