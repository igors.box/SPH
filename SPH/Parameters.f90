module Parameters
implicit none
!---------------------------------------------------------------------------------------------------

!*Model parameters*!
! T_out = 11600 К, rho_out = 1.67d-5 см^-3

real(8), public, parameter :: V_pl = 3.0d6 				! Planet velocity
real(8), public, parameter :: R_pl = 7.0d9				! Planet radius
real(8), public, parameter :: M_pl = 1.9d30				! Planet mass

real(8), public, parameter :: rho_out = 1d-12				! Density of unperturbed star atmosphere
real(8), public, parameter :: T_out = 2000d0 				! Temperature of unperturbed star atmosphere
real(8), public, parameter :: p_out = 8.3144621d7 * rho_out * T_out	! Pressure of unperturbed star atmosphere
real(8), public, parameter :: h_out = 2.50d0*p_out / rho_out  		! Enthalpy of unperturbed star atmosphere


!---------------------------------------------------------------------------------------------------

!*Physical constants*!

real(8), public, parameter :: pi_const = 3.14159265359			! Number pi
real(8), public, parameter :: R_gas_const = 8.3144621d7 		! Universal gas constant
real(8), public, parameter :: k_B_const = 1.3806488d-16 		! Boltzmann constant
real(8), public, parameter :: sigma_const = 5.670367d-5			! Stefen-Boltzmann constant
real(8), public, parameter :: c_const = 2.99792458d10			! Speed of light
real(8), public, parameter :: m_e_const = 9.10938291d-28		! Electron mass
real(8), public, parameter :: m_H_const = 1.6735327d-24			! Hydrogen mass
real(8), public, parameter :: E1_H_const = 2.17896d-11    		! Hydrogen ionisation first level
real(8), public, parameter :: h_const = 6.62606957d-27			! Planck constant
real(8), public, parameter :: G_grav_const = 6.67408d-8			! Gravity constant
real(8), public, parameter :: E_e_const = 4.80320427d-10		! Charge of electron
real(8), public, parameter :: Poisson_const = 5d0 / 3d0

!---------------------------------------------------------------------------------------------------

!обезразмеренные величины
real(8), public, parameter :: R_pl1 = 1d0
real(8), public, parameter :: T_out1 = 1d0
real(8), public, parameter :: V_pl1 = V_pl *dsqrt(rho_out/p_out)
real(8), public, parameter :: rho_out1 = 1d0
real(8), public, parameter :: p_out1 = 1d0 

real(8), public, parameter :: R_gas_const1 = R_gas_const *T_out/p_out/(R_pl**3)
real(8), public, parameter :: mu1 = 1d0 / rho_out / (R_pl**3)
real(8), public, parameter :: k_B_const1 = R_gas_const1 / 6.022d23
real(8), public, parameter :: m_H_const1 = m_H_const / rho_out / (R_pl**3)
real(8), public, parameter :: G_grav_const1 = G_grav_const * rho_out**2 * R_pl**2 / p_out
real(8), public, parameter :: M_pl1 = M_pl / rho_out / (R_pl**3)
real(8), public, parameter :: E1_H_const1 = E1_H_const / p_out / R_pl**3
real(8), public, parameter :: h_const1 = h_const / R_pl**4 * dsqrt(1d0 / rho_out / p_out)
real(8), public, parameter :: E_e_const1 = E_e_const / R_pl**2 / dsqrt(p_out)
real(8), public, parameter :: c_const1 = c_const * dsqrt(rho_out / p_out)
real(8), public, parameter :: m_e_const1 = m_e_const / rho_out / R_pl**3
real(8), public, parameter :: sigma_const1 = sigma_const * T_out**4 / p_out * dsqrt(rho_out / p_out)

real(8), public, parameter :: u_out1 = p_out1 / rho_out1 / (5d0/3d0 - 1d0)
real(8), public, parameter :: ksi_out1 = 4d0 * sigma_const1 / (c_const1 * rho_out1) * T_out1**4
real(8), public, parameter :: c_v_i1 = R_gas_const1 / (Poisson_const - 1d0) / mu1 !теплоемкость газа

end module parameters
