#include "SPHConfig.f90"
    
module DataWriter
    use :: MpiHelpers
    use :: SPH

    implicit none
#ifdef CALCULATE_PARTICLE_INTERACTION_COUNT
    character(*), parameter, private :: formatter = "(F20.10, 1x,F20.10, 1x,F20.10, 1x,F20.10, 1x,F20.10, 1x,F20.10, 1x,F20.10, F20.10, 1x, I6)"
#else
    character(*), parameter, private :: formatter = "(F20.10, 1x,F20.10, 1x,F20.10, 1x,F20.10, 1x,F20.10, 1x,F20.10, 1x,F20.10, F20.10)"
#endif
    real(8), parameter, private :: D_QNAN = transfer((/ Z'00000000', Z'7FF80000' /), 1.0d8)

    contains

#ifdef OUTPUT_UNIFORM_GRID
#ifdef UNIFORM_GRID_SIMPLE_SMOOTHING
    subroutine GetDataUniformGrid(x, y, v, rho, p, T_gas, T_rad, interactionCount)
        real(8), intent(in) :: x, y
        real(8), dimension(2), intent(out) :: v
        real(8), intent(out) :: rho, p, T_gas, T_rad
        integer(4) :: interactionCount, i, a
        real(8) :: W
        real(8), dimension(2) :: r
        
        r(1) = x
        r(2) = y
    
        interactionCount = 0
        rho = 0d0
        p = 0d0
        v = 0d0
        T_gas = 0d0
        T_rad = 0d0
        do i=1, interactParticles(0)
            a = interactParticles(i)
     
            if(VecNorm(r - point(a)  %  r) >= h1) then
                cycle
            endif
                    
            W = GetW(r - point(a)  %  r)

            if(W /= 0d0) then
                interactionCount = interactionCount + 1
                rho = rho + point(a)  %  rho
                p = p + point(a)  %  p
                v = v + point(a)  %  v
                T_gas = T_gas + point(a)  %  T_gas
                T_rad = T_rad + point(a)  %  T_rad
            endif
        enddo
                    
        W = VecNorm(r)
        W = GetBodyIntersectCorrection(W, h1, hLimit1)
                    
        if(interactionCount == 0) then
            rho = D_QNAN
            p = D_QNAN
            v = D_QNAN
            T_gas = D_QNAN
            T_rad = D_QNAN
        else
            rho = rho / float(interactionCount) * W
            p = p / float(interactionCount) * W
            v = v / float(interactionCount) * W
            T_gas = T_gas / float(interactionCount) * W
            T_rad = T_rad / float(interactionCount) * W
        endif      
        
    end subroutine
#else
    subroutine GetDataUniformGrid(x, y, v, rho, p, T_gas, T_rad, interactionCount)
        real(8), intent(in) :: x, y
        real(8), dimension(2), intent(out) :: v
        real(8), intent(out) :: rho, p, T_gas, T_rad
        integer(4) :: interactionCount, i, a
        real(8) :: W
        real(8), dimension(2) :: r
        
        r(1) = x
        r(2) = y
        
        interactionCount = 0
        rho = 0d0
        p = 0d0
        v = 0d0
        T_gas = 0d0
        T_rad = 0d0
        do i=1, interactParticles(0)
            a = interactParticles(i)
                        
            W = GetW(r - point(a)  %  r)

            if(W /= 0d0) then
                interactionCount = interactionCount + 1                            
                rho = rho + W                            
                p = p + 1d0 / point(a)  %  rho * point(a)  %  p * W
                v = v + 1d0 / point(a)  %  rho * point(a)  %  v * W
                T_gas = T_gas + 1d0 / point(a)  %  rho * point(a)  %  T_gas * W
                T_rad = T_rad + 1d0 / point(a)  %  rho * point(a)  %  T_rad * W
            endif
        enddo
                    
        W = VecNorm(r)
        W = GetPlanetIntersectCorrection(W)                    
        rho = rho * m1 * W
        p = p * m1 * W
        v = v * m1 * W
        T_gas = T_gas * m1 * W
        T_rad = T_rad * m1 * W
    end subroutine
#endif

    subroutine WriteData(file)
        integer(4), intent(in) :: file
        integer(4) :: interactionCount
        real(8) :: rho, p, T_gas, T_rad
        real(8), dimension(2) :: v
        real(8) :: x, y
        real(8) :: uniformGridStep

        if(MpiRank == 0) then
            write(file, "(A)") "#r(i, 1), r(i, 2), v(i,1), v(i,2), rho(a), p(a), T_gas, T_radiation"
        endif
        
        uniformGridStep = dsqrt(((x_max1 - x_min1) * (y_max1 - y_min1) - pi_const*R_pl1**2) / float(OUTPUT_UNIFORM_GRID_N))
        x = x_min1
        do while(x <= x_max1)
            y = y_min1 + ceiling((bottomBound1 - y_min1) / uniformGridStep) * uniformGridStep
            do while(y <= upperBound1)
                
                if(x >= UNIFORM_GRID_X_MIN .and. x <= UNIFORM_GRID_X_MAX) then
                    if(y >= UNIFORM_GRID_Y_MIN .and. y <= UNIFORM_GRID_Y_MAX) then
                        if(x**2 + y**2 > R_pl1**2) then
                            call GetDataUniformGrid(x, y, v, rho, p, T_gas, T_rad, interactionCount)    
#ifdef CALCULATE_PARTICLE_INTERACTION_COUNT
                            write(file, formatter) x, y, v(1), v(2), rho, p, T_gas, T_rad, interactionCount
#else
                            write(file, formatter) x, y, v(1), v(2), rho, p, T_gas, T_rad
#endif
                        endif
                    endif
                endif              
                y = y + uniformGridStep
            enddo
            x = x + uniformGridStep
        enddo
      
    end subroutine
#else
    subroutine WriteData(file)
        integer(4) :: i, a
        integer(4), intent(in) :: file

        if(MpiRank == 0) then
            write(file, "(A)") "#r(i, 1), r(i, 2), v(i,1), v(i,2), rho(a), p(a), T_gas, T_radiation"
        endif

        do i=1, calculateParticles(0)
            a = calculateParticles(i)                
#ifdef CALCULATE_PARTICLE_INTERACTION_COUNT
            write(file, formatter) point(a) % r(1), point(a) % r(2), point(a) % v(1), point(a) % v(2), point(a) % rho, point(a) % p,  point(a) % T_gas,  point(a) % T_rad, point(a) % interactionCount
#else
            write(file, formatter) point(a) % r(1), point(a) % r(2), point(a) % v(1), point(a) % v(2), point(a) % rho, point(a) % p,  point(a) % T_gas,  point(a) % T_rad
#endif
        enddo
    end subroutine
#endif
    
end module
    
    