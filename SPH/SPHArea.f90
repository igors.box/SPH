#include "SPHConfig.f90"
    
module SPHArea
    use :: Parameters
    use :: MpiTransferPoint
    use :: MpiHelpers
    use :: RealArray
    use :: Helpers

    implicit none
    
    type(RealArrayType), private :: NewParticlesBroadcast
    type(PointType), private :: newPoint
    
    real(8) :: dNiterate

    integer(4), private, parameter :: N = PARTICLES_COUNT
    real(8), private, parameter :: x_min1 = AREA_X_MIN
    real(8), private, parameter :: x_max1 = AREA_X_MAX
    real(8), private, parameter :: y_min1 = AREA_Y_MIN
    real(8), private, parameter :: y_max1 = AREA_Y_MAX
        
    contains
    
    function GravityAccelerationDV(r) result(dv)
        real(8), dimension(2), intent(in) :: r
        real(8), dimension(2) :: dv
            
        !-GM_pl * r /  |r|^3
        dv = - G_grav_const1 * M_pl1 * r / VecNorm(r)**3
    end function
    
    subroutine SPHAreaInitialize()
        NewParticlesBroadcast % defaultAllocationSize = N / 100
        NewParticlesBroadcast % expandMultiplyer = 2
        
        newPoint % r(1) = x_min1
#ifdef DEBUG
        newPoint % p = -1d0 !��� ������, ������ ���� ����������� ����� �������
        newPoint % T_gas = -1d0 !��� ������, ������ ���� ����������� ����� �������
        newPoint % T_rad = -1d0 !��� ������, ������ ���� ����������� ����� �������
        newPoint % rho = -1d0 !��� ������, ������ ���� ����������� ����� �������
#endif
        newPoint % u = u_out1 !����������� �������� E
        newPoint % ksi = ksi_out1 !����������� �������� ��� ksi
        newPoint % v(1) = V_pl1
        newPoint % v(2) = 0d0
        newPoint % dr = 0d0
#ifdef CALCULATE_PARTICLE_INTERACTION_COUNT
        newPoint % interactionCount = 0
#endif

    end subroutine
    
    subroutine ParticlesCreator(dt1, AddPointCallBack)
        implicit none

        interface
        subroutine AddPointCallBack(pointToAdd)
            use :: MpiTransferPoint
            type(PointType), intent(in) :: pointToAdd
        end subroutine
        end interface
        
        real(8), intent(in) :: dt1
        
        integer(4) :: i, mpiError, dNCount
        real(8) :: y, dN1
        
        dN1 = N * V_pl1 * dt1 / (x_max1 - x_min1)
        
        !�������� ���������� ��������� ������ � ���������� �������
        dNiterate = dNiterate + dN1
        dNCount = int(dNiterate)
        dNiterate = dNiterate - dNCount
                    
        call NewParticlesBroadcast % Expand(dNCount)
        
        !������ ����� ������� �� �����
        if(MpiRank == 0) then
            do i=1, dNCount
                call random_number(y)
                y = y * (y_max1 - y_min1) - (y_max1 - y_min1) / 2d0
                NewParticlesBroadcast % Instance(i) = y
            enddo
        endif

        !��������� Y ���������� ��� ������ ����� ������� ����
        call mpi_bcast(NewParticlesBroadcast % Instance, dNCount, MPI_REAL8, 0, MPI_COMM_WORLD, mpiError)
        call AssertMpiError(mpiError)

        !������ ���������
        do i = 1, dNCount
            y = NewParticlesBroadcast % Instance(i)
            newPoint % r(2) = y
            call AddPointCallBack(newPoint)
        enddo
        
    end subroutine
    
    subroutine ParticleMover(point)
        type(PointType), intent(inout) :: point
        
        real(8) :: x, y, dX, dY, newX, newY, vX, vY
        
        !����������� ��������            
        x = point % r(1)
        y = point % r(2)
        dX = point % dr(1)
        dY = point % dr(2)
        newX = x + dX
        newY = y + dY
        vX = point % v(1)
        vY = point % v(2)
            
        !��������� ������ �� ����
        !���������� ��������� �� ������� "������" �������
        if(newX**2 + newY**2 <= R_pl1**2) then
            call InteractWithBody(x, y, dX, dY, newX, newY, vX, vY)
        else
            x = newX
            y = newY
        endif
            
        if(x**2 + y**2 <= R_pl1**2) then
            write(*, *) "Point inside the body after reflection"
        endif

        point % r(1) = x
        point % r(2) = y
        point % v(1) = vX
        point % v(2) = vY

    end subroutine
    
    subroutine InteractWithBody(arg_x, arg_y, arg_dX, arg_dY, newX, newY, vX, vY)
        real(8), intent(in) :: arg_x, arg_y, arg_dX, arg_dY
        real(8), intent(inout) :: newX, newY, vX, vY

        real(8) :: x, y, dX, dY
        real(8) :: temp
        real(8) :: lineK, lineB, borderX, borderY
        real(8) :: D, borderX1, borderX2, borderY1, borderY2, lenToBorder1, lenToBorder2
        real(8) :: lenToBorder, lineLen
        logical :: isRotated
        real(8) :: borderTheta
        
        x = arg_x
        y = arg_y
        dX = arg_dX
        dY = arg_dY
    
        !�������� ��� ��������� ������� �� 90 �������� ����� dX ��� (��� 0)
        if(dabs(dX) < dabs(dY)) then
            temp = x * dcos(pi_const / 2d0) - y * dsin(pi_const / 2d0)
            y =    x * dsin(pi_const / 2d0) + y * dcos(pi_const / 2d0)
            x = temp
                    
            temp = newX * dcos(pi_const / 2d0) - newY * dsin(pi_const / 2d0)
            newY = newX * dsin(pi_const / 2d0) + newY * dcos(pi_const / 2d0)
            newX = temp
                    
            temp = vX * dcos(pi_const / 2d0) - vY * dsin(pi_const / 2d0)
            vY =   vX * dsin(pi_const / 2d0) + vY * dcos(pi_const / 2d0)
            vX = temp
                    
            dX = newX - x
            dY = newY - y
                    
            isRotated = .true.
        else
            isRotated = .false.
        endif
                    
        !����� �����
        lineLen = dsqrt(dX**2 + dY**2)
        !��������� ������������ y=kx + b ��� ����� �������
        if(dabs(dX) < dabs(dY)) then
            call MpiAbortWithMessage("Invalid reflection: (dabs(dX) < dabs(dY))")
        endif
                
        !��������� �����
        lineK = dY / dX
        lineB = y-lineK * x

        !���������� ����������� � ��������
        D = (lineK * lineB)**2 - (lineK**2 + 1d0) * (lineB**2-R_pl1**2)
        if(D < 0d0) then
            D = 0d0
        endif
        D = dsqrt(D)
        borderX1 = (-lineK * lineB + D) / (lineK**2 + 1d0)
        borderX2 = (-lineK * lineB-D) / (lineK**2 + 1d0)
        borderY1 = borderX1 * lineK + lineB
        borderY2 = borderX2 * lineK + lineB
        !����� ����� �� �����������
        lenToBorder1 = dsqrt((borderX1 - x)**2 + (borderY1 - y)**2)
        lenToBorder2 = dsqrt((borderX2 - x)**2 + (borderY2 - y)**2)
        !������� ��������� �����������
        if(lenToBorder1 < lenToBorder2) then
            lenToBorder = lenToBorder1
            borderX = borderX1
            borderY = borderY1
        else
            lenToBorder = lenToBorder2
            borderX = borderX2
            borderY = borderY2
        endif                    
                
        !�������� �������� ���������� Theta ����� ����������� ���������� � ������
        borderTheta = datan(borderY / borderX)
        if(borderX < 0) then
            borderTheta = borderTheta + pi_const
        endif
                
        !������� x,y � vX, vY ����� ������� ��������� ��� ���� Theta
        temp = x * dcos(2d0 * borderTheta) + y * dsin(2d0 * borderTheta)
        y =    x * dsin(2d0 * borderTheta) - y * dcos(2d0 * borderTheta)
        x = temp                
 
        temp =  -(vX * dcos(2d0 * borderTheta) + vY * dsin(2d0 * borderTheta))
        vY =    -(vX * dsin(2d0 * borderTheta) - vY * dcos(2d0 * borderTheta))
        vX = temp  
                
        !�������� x � y �� ������� �����
        if(lenToBorder /= 0d0) then
            x = borderX + (x - borderX) * (lineLen - lenToBorder) / lenToBorder
            y = borderY + (y - borderY) * (lineLen - lenToBorder) / lenToBorder
        else
            x = borderX + (x - borderX) * lineLen
            y = borderY + (y - borderY) * lineLen
        endif                    
                
        !�������� ����������� ��������� ������� � �������� ��
        if(isRotated) then
            temp = x * dcos(-pi_const / 2d0) - y * dsin(-pi_const / 2d0)
            y =    x * dsin(-pi_const / 2d0) + y * dcos(-pi_const / 2d0)
            x = temp
                    
            temp = vX * dcos(-pi_const / 2d0) - vY * dsin(-pi_const / 2d0)
            vY =   vX * dsin(-pi_const / 2d0) + vY * dcos(-pi_const / 2d0)
            vX = temp                    
        endif
    end subroutine
    
    function GetBodyIntersectCorrection(dist, h1, hLimit1) result(correction)
        real(8), intent(in) :: dist, h1, hLimit1
        real(8) :: correction
        real(8) :: planetIntersectSquare
        real(8) :: x_in !������ ����� �������������� ������� ��� ����������� � ������������� �� h1
        real(8) :: int_W_0_x !�������� �� ���� W �� 0 �� x_in
        real(8) :: int_intersect !������������� �������� �� ���� �� ������� ���������� (�������������)
        
        planetIntersectSquare = IntersectionCirclesSquare(R_pl1, hLimit1, dist)
        if(planetIntersectSquare == 0d0) then
            correction = 1d0
            return
        endif
        
        x_in = (dist - R_pl1) / h1
        
        if(x_in <= 1d0) then
            int_W_0_x = 10d0 / 7d0 * (3d0 * x_in**5 / 10d0 - 3d0 * x_in**4 / 4d0 + x_in**2)
        else
            int_W_0_x = -1d0 / 7d0 + 5d0 / 7d0 * (-x_in**5 / 5d0 + 3d0 * x_in**4 / 2d0 - 4d0 * x_in**3 + 4d0 * x_in**2)
        endif
        
        int_intersect = planetIntersectSquare * (1d0 - int_W_0_x) / pi_const / h1**2 / (4d0 - x_in**2)
        correction = (int_W_0_x + (1d0 - int_W_0_x)**2 / (1d0 - int_W_0_x - int_intersect)) !������������� ���������
    end function
    
end module
    