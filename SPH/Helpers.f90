module Helpers
    use :: Parameters
	implicit none
    contains

    function VecNorm(x) result(n)
        real(8), dimension(2), intent(in) :: x
        real(8) :: n
        n = dsqrt(x(1) * x(1) + x(2) * x(2)) 
    end function
    
    function sign(x) result(s)
        real(8), intent(in) :: x
        real(8) :: s
        if(x >= 0d0) then
            s = 1d0
        else
            s = -1d0
        endif
    end function
    
    function IntersectionCirclesSquare(r1, r2, dist) result(res) 
        real(8) :: square1, square2, factor, minR, maxR
        real(8) :: res
        real(8), intent(in) :: dist, r1, r2

        if (r1 + r2 > dist) then 
            minR = min(r1, r2)
            maxR = max(r1, r2)

            if ((dist + minR >= maxR) .and. (dist /= 0d0)) then 
                square1 = r1**2 * dacos((dist**2 + r1**2 - r2**2)/(2d0 * dist * r1))
                square2 = r2**2 * dacos((dist**2 + r2**2 - r1**2)/(2d0 * dist * r2))
                factor = 0.5d0 * dsqrt((-dist+r1+r2)*(dist-r1+r2)*(dist+r1-r2)*(dist+r1+r2))
                res = square1 + square2 - factor
            else
                !Площадь польностью накрытой минимальной окружности
                res = pi_const * minR**2
            end if
        else
            !Не пересекаются 
            res = 0d0
        end if
    end function    
end module