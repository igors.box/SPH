!выводить в файл количество взаимодействий частиц
!#define CALCULATE_PARTICLE_INTERACTION_COUNT
!использовать кластеризацию
#define USE_CLUSTER_MODE
!использовать динамическую коррекцию dt
#define DYNAMIC_DT
!включает некоторый отладочный код
!#define DEBUG

!приблизительное количество частиц в SPH
#define PARTICLES_COUNT 30000
!X координата левого нижнего угла (нормированная на радиус планеты)
#define AREA_X_MIN -3d0
!X координата правого верхнего угла (нормированная на радиус планеты)
#define AREA_X_MAX  5d0
!Y координата левого нижнего угла (нормированная на радиус планеты)
#define AREA_Y_MIN -5d0
!Y координата правого верхнего угла (нормированная на радиус планеты)
#define AREA_Y_MAX  5d0

!Тоность итерационного метода поиска энергий газа и излучения
#define INTERATION_PRECISE (1d-2)
!Лимит интеграла Росселандова среднего
#define ROSSELAND_MEAN_LIMIT (100d0)

!X координата левого нижнего угла обсчета светимости (нормированная на радиус планеты)
#define LUMINOSITY_WINDOW_X_MIN (AREA_X_MIN + 1d0)
!X координата правого верхнего угла обсчета светимости (нормированная на радиус планеты)
#define LUMINOSITY_WINDOW_X_MAX (AREA_X_MAX - 1d0)
!Y координата левого нижнего угла обсчета светимости (нормированная на радиус планеты)
#define LUMINOSITY_WINDOW_Y_MIN (AREA_Y_MIN + 1d0)
!Y координата правого верхнего угла обсчета светимости (нормированная на радиус планеты)
#define LUMINOSITY_WINDOW_Y_MAX (AREA_Y_MAX - 1d0)

!количество файлов с результатами перед остановкой SPH
#define WRITE_FILE_COUNT 3000
!количество итераций на один файл с результатами
#define WRITE_DATA_STEP 1

!включить восстанавливаемое сохранение итерации (перезаписывают друг друга)
#define BACKUP_ENABLED
!количество итераций между сохранениями
#define BACKUP_STEP 200

!включить остановку SPH по времени
!#define STOP_IN_TIME
!час остановки
!#define STOP_HOUR 9

!остановка SPH при создании в текущей дериктории файла stop
#define USE_STOP_FILE

!файл с данными сохраняется по равномерной сетку (сглаживание по ядру)
#define OUTPUT_UNIFORM_GRID
!примерное количество точек в равномерной сетке
#define OUTPUT_UNIFORM_GRID_N 100000
#define UNIFORM_GRID_X_MIN -3d0
#define UNIFORM_GRID_Y_MIN -3d0
#define UNIFORM_GRID_X_MAX 4.5d0
#define UNIFORM_GRID_Y_MAX 3d0
#define UNIFORM_GRID_SIMPLE_SMOOTHING