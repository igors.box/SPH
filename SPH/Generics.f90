!ArrayCollection<integer(4)>
#define GENERIC_TYPE integer(4)
#define MODULE_NAME IntegerArray
#define COLLECTION_TYPE_NAME IntegerArrayType
#include "ArrayCollection.f90"
#undef MODULE_NAME
#undef COLLECTION_TYPE_NAME
#undef GENERIC_TYPE

!PoolCollection<integer(4)>
#define GENERIC_TYPE integer(4)
#define MODULE_NAME IntegerPool
#define COLLECTION_TYPE_NAME IntegerPoolType
#include "PoolCollection.f90"
#undef MODULE_NAME
#undef COLLECTION_TYPE_NAME
#undef GENERIC_TYPE

!ArrayCollection<real(8)>
#define GENERIC_TYPE real(8)
#define MODULE_NAME RealArray
#define COLLECTION_TYPE_NAME RealArrayType
#include "ArrayCollection.f90"
#undef MODULE_NAME
#undef COLLECTION_TYPE_NAME
#undef GENERIC_TYPE