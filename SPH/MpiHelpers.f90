module MpiHelpers
    use :: Mpi
    implicit none
    
    integer(4) :: MpiRank, MpiSize
    
    contains
    
    subroutine AssertMpiError(err)
    integer(4), intent(in) :: err
        if(err /= MPI_SUCCESS) then
            write(*,*) "MPI Assertion failed with code:", err
            call MpiAbort()
        endif
    end subroutine

    subroutine MpiAssert(assertion)
    logical, intent(in) :: assertion
        if(.not.assertion) then
            call MpiAbortWithMessage("Assertion fail!")
        endif
    end subroutine
    
    subroutine MpiAbort()
    integer(4) :: err, errorCode
        write(*,*) "Aborting..."
        errorCode = 1
        call MPI_Abort(MPI_COMM_WORLD, errorCode, err)
        if(err /= MPI_SUCCESS) then
            stop
        endif
	end subroutine

    subroutine MpiAbortWithMessage(message)
	character(len=*), intent(in) :: message
		write(*,*) message
		call MpiAbort()
    end subroutine
	
    subroutine MpiInit()
    integer(4) :: mpi_err
        call mpi_init(mpi_err)
        call AssertMpiError(mpi_err)
        call mpi_comm_size(MPI_COMM_WORLD, MpiSize, mpi_err)
        call AssertMpiError(mpi_err)
        call mpi_comm_rank(MPI_COMM_WORLD, MpiRank, mpi_err)
        call AssertMpiError(mpi_err)
    end subroutine
    
    subroutine MpiFinalize()
    integer(4) :: mpi_err
        call mpi_finalize(mpi_err)
        call AssertMpiError(mpi_err)
    end subroutine
    
    subroutine MpiBarrier()
    integer(4) :: mpi_err
        call mpi_barrier(MPI_COMM_WORLD, mpi_err)
        call AssertMpiError(mpi_err)
    end subroutine
    
    subroutine MpiExchangeTwoIntegers(upperSendValue, bottomSendValue, upperRecvValue, bottomRecvValue)
        integer(4), parameter :: MpiUpperValueTag = 5001, MpiBottomValueTag = 5002
        integer(4), intent(in) :: upperSendValue, bottomSendValue
        integer(4), intent(out) :: upperRecvValue, bottomRecvValue
        
        external Mpi_WaitAll
        
        integer(4), dimension(4) :: MpiRequests
        integer(4) :: mpiError

        !������ ��������
        MpiRequests = MPI_REQUEST_NULL

        !�������� ������ �����
        if(MpiRank /= 0) then
            call mpi_isend(upperSendValue, 1, MPI_INTEGER4, MpiRank - 1, MpiUpperValueTag, MPI_COMM_WORLD, MpiRequests(1), mpiError)
            call AssertMpiError(mpiError)
        endif
    
        !�������� ������ ����
        if(MpiRank /= MpiSize - 1) then
            call mpi_isend(bottomSendValue, 1, MPI_INTEGER4, MpiRank + 1, MpiBottomValueTag, MPI_COMM_WORLD, MpiRequests(2), mpiError)
            call AssertMpiError(mpiError)
        endif
          
        !�������� ������ �����
        if(MpiRank /= MpiSize - 1) then
            call mpi_irecv(bottomRecvValue, 1, MPI_INTEGER4, MpiRank + 1, MpiUpperValueTag, MPI_COMM_WORLD, MpiRequests(3), mpiError)
            call AssertMpiError(mpiError)
        else
            bottomRecvValue = 0
        endif
    
        !�������� ������ ������
        if(MpiRank /= 0) then
            call mpi_irecv(upperRecvValue, 1, MPI_INTEGER4, MpiRank - 1, MpiBottomValueTag, MPI_COMM_WORLD, MpiRequests(4), mpiError)
            call AssertMpiError(mpiError)
        else
            upperRecvValue = 0
        endif
      
        call Mpi_WaitAll(4, MpiRequests, MPI_STATUS_IGNORE, mpiError)
        call AssertMpiError(mpiError)
        
    end subroutine        

    subroutine MpiExchangeTwoIndexedSubArrays(targetArray, upperSendIndexes, bottomSendIndexes, upperRecvIndexes, bottomRecvIndexes, MpiType)
        use :: IntegerArray
        use :: iso_c_binding

        type(*), dimension(:), intent(in) :: targetArray
        integer(4) :: upperSendTag, bottomSendTag, upperRecvTag, bottomRecvTag
        integer(4), dimension(4) :: MpiRequests
        integer(4) :: mpiError
        integer(4) :: MPI_TYPE_UpperSend, MPI_TYPE_BottomSend, MPI_TYPE_UpperRecv, MPI_TYPE_BottomRecv
        integer(4) :: upperSendCount, bottomSendCount, upperRecvCount, bottomRecvCount

        external Mpi_WaitAll
        
        type(IntegerArrayType), intent(in) :: upperSendIndexes, bottomSendIndexes, upperRecvIndexes, bottomRecvIndexes
        integer(4), intent(in) :: MpiType
        
        upperSendTag = mod(abs(MpiType), 1000) + 1000
        bottomRecvTag = upperSendTag
        bottomSendTag = upperSendTag + 1
        upperRecvTag = bottomSendTag

        upperSendCount = upperSendIndexes % Cursor
        bottomSendCount = bottomSendIndexes % Cursor
        upperRecvCount = upperRecvIndexes % Cursor
        bottomRecvCount = bottomRecvIndexes % Cursor

        !������ ��������
        MpiRequests = MPI_REQUEST_NULL
    
        !�������� �����
        if(MpiRank /= 0 .and. upperSendCount /= 0) then
            
            call MPI_Type_create_indexed_block(upperSendCount, 1, upperSendIndexes % Instance, MpiType, MPI_TYPE_UpperSend, mpiError)
            call AssertMpiError(mpiError)
            call mpi_type_commit(MPI_TYPE_UpperSend, mpiError)
            call AssertMpiError(mpiError)

            call mpi_isend(targetArray, 1, MPI_TYPE_UpperSend, MpiRank - 1, upperSendTag, MPI_COMM_WORLD, MpiRequests(1), mpiError)
            call AssertMpiError(mpiError)
        endif

        !�������� ����
        if(MpiRank /= MpiSize - 1 .and. bottomSendCount /= 0) then
            
            call MPI_Type_create_indexed_block(bottomSendCount, 1, bottomSendIndexes % Instance, MpiType, MPI_TYPE_BottomSend, mpiError)
            call AssertMpiError(mpiError)
            call mpi_type_commit(MPI_TYPE_BottomSend, mpiError)
            call AssertMpiError(mpiError)
            
            call mpi_isend(targetArray, 1, MPI_TYPE_BottomSend, MpiRank + 1, bottomSendTag, MPI_COMM_WORLD, MpiRequests(2), mpiError)
            call AssertMpiError(mpiError)
        endif
               
        !�������� ������
        if(MpiRank /= 0 .and. upperRecvCount /= 0) then
            call MPI_Type_create_indexed_block(upperRecvCount, 1, upperRecvIndexes % Instance, MpiType, MPI_TYPE_UpperRecv, mpiError)
            call AssertMpiError(mpiError)
            call mpi_type_commit(MPI_TYPE_UpperRecv, mpiError)
            call AssertMpiError(mpiError)
            
            call mpi_irecv(targetArray, 1, MPI_TYPE_UpperRecv, MpiRank - 1, upperRecvTag, MPI_COMM_WORLD, MpiRequests(3), mpiError)
            call AssertMpiError(mpiError)
            call mpi_type_free(MPI_TYPE_UpperRecv, mpiError)
            call AssertMpiError(mpiError)
        endif
    
        !�������� �����
        if(MpiRank /= MpiSize - 1 .and. bottomRecvCount /= 0) then
            
            call MPI_Type_create_indexed_block(bottomRecvCount, 1, bottomRecvIndexes % Instance, MpiType, MPI_TYPE_bottomRecv, mpiError)
            call AssertMpiError(mpiError)
            call mpi_type_commit(MPI_TYPE_bottomRecv, mpiError)
            call AssertMpiError(mpiError)
            call mpi_irecv(targetArray, 1, MPI_TYPE_bottomRecv, MpiRank + 1, bottomRecvTag, MPI_COMM_WORLD, MpiRequests(4), mpiError)
            call AssertMpiError(mpiError)

            call mpi_type_free(MPI_TYPE_bottomRecv, mpiError)
            call AssertMpiError(mpiError)
        endif
        
        call Mpi_WaitAll(4, MpiRequests, MPI_STATUS_IGNORE, mpiError)
        call AssertMpiError(mpiError)
        
        if(MpiRank /= 0 .and. upperSendCount /= 0) then
            call mpi_type_free(MPI_TYPE_UpperSend, mpiError)
            call AssertMpiError(mpiError)
        endif
        
        if(MpiRank /= MpiSize - 1 .and. bottomSendCount /= 0) then
            call mpi_type_free(MPI_TYPE_BottomSend, mpiError)
            call AssertMpiError(mpiError)
        endif
        
    end subroutine
    
end module
    