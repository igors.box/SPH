set terminal pngcairo size 1200,600 enhanced font 'Verdana,10'
set output sprintf("%s.png",filename)
load "data/vars.gnu"
R_pl=1
set title
set xtics R_pl
set ytics R_pl
set size ratio -1
set cbrange [1.1:1.4]
plot [x_min1:x_max1][y_min1:y_max1] 0 notitle, [-R_pl:R_pl] (R_pl**2-x**2)**0.5 notitle, [-R_pl:R_pl] -(R_pl**2-x**2)**0.5 notitle, filename using 1:2:7 w points palette pt 8 ps 0.61 title ""


#set view map
#splot filename using 1:2:7 with points palette title "T"

#set pm3d depthorder
#set pm3d interpolate 0,0
#splot filename using 1:2:7 title "T"

#set pm3d interpolate 0,0
#set pm3d map
#splot filename using 1:2:(log10($4+10.)) title "T"