#include "SPHConfig.f90"

module Main
    implicit none
    integer(4) :: fileCount, iterationCount, backupCount
    
    contains
    
    subroutine PrepareDirectories()
        use :: MpiHelpers
        character(2) :: rankchar
        integer(4) :: error
        logical :: isLinux
        
        if(MpiRank < 10) then
            write(rankchar,"(I1)") MpiRank
        else
            write(rankchar,"(I2)") MpiRank
        endif
        
        !проверим таким нехитрым способом какая мы OS
        open(1, FILE="/dev/random", STATUS="old", iostat=error)
        if(error == 0) then
            close(1)
        endif
        isLinux = error == 0
        
        if(isLinux) then
            call execute_command_line("mkdir data &> /dev/null")
            call execute_command_line("mkdir backup &> /dev/null")
            call execute_command_line("mkdir data/"//trim(adjustl(rankchar)) // " &> /dev/null")
            call execute_command_line("mkdir backup/"//trim(adjustl(rankchar)) // " &> /dev/null")
        else
            call execute_command_line("mkdir data\"//trim(adjustl(rankchar)) // " > NUL 2>&1")
            call execute_command_line("mkdir backup\"//trim(adjustl(rankchar)) // " > NUL 2>&1")
        endif
    end subroutine
    
    function NeedToStop(fileCount) result(res)
        integer(4), intent(in) :: fileCount
        logical :: res
#ifdef STOP_IN_TIME
        integer(4), dimension(8) :: time
#endif
        
        res = .false.
        
        if(fileCount >= WRITE_FILE_COUNT) then
            res = .true.
            return
        endif
        
#ifdef USE_STOP_FILE
        inquire(file="stop", exist = res)
        if(res) then
            return
        endif
#endif

#ifdef STOP_IN_TIME
        call date_and_time(values = time)
        res = (time(5) == STOP_HOUR)
#endif
    end function

#ifdef BACKUP_ENABLED
    function CheckNeedRestore() result(needRestore)
        logical :: needRestore
        character(100) :: argument
        
        needRestore = .false.
        if(Command_argument_count() > 0) then
            call Get_command_argument(1, argument)
            if(trim(adjustl(argument)) == "restore") then
                needRestore = .true.
            endif
        endif
    end function
#endif
    
    subroutine Go()
        use :: SPH
        use :: MpiHelpers
#ifdef BACKUP_ENABLED
        use :: Backup
#endif
        integer(4) :: mpiError
        logical :: stopCommand = .false.

        write(*,*) "Start Mpi"
        call MpiInit()
        write(*,*) "MPI_COMM_SIZE:", MpiSize, "CURRENT_RANK:", MpiRank

        call PrepareDirectories()
        
        write(*,*) "Start SPH"
        call SPHInit()

        iterationCount = 0
            
#ifdef BACKUP_ENABLED
        backupCount = 0
        if(CheckNeedRestore()) then
            write(*,*) "Restore iteration..."
            call RestoreIteration(iterationCount, fileCount)
        endif
#endif
        
        if(MpiRank == 0) then
            write(*,*) "h1=", h1
            write(*,*) "dt1=", dt1
            write(*,*) "m1=", m1
            
            open(unit=1,file="data/vars.gnu", status='replace')
            write(1,*) "x_min1=", x_min1
            write(1,*) "x_max1=", x_max1
            write(1,*) "y_min1=", y_min1
            write(1,*) "y_max1=", y_max1
        
            close(1)
        endif
              
        call MpiBarrier()

        !fileCount = 0
        !call UpdatePointParameters()
        !call MpiBarrier()
        !call WriteToFile()
        !call MpiBarrier()
        !stop 0
        
        do 
            call SPHIteration()

            iterationCount = iterationCount + 1
            if(mod(iterationCount, WRITE_DATA_STEP) == 0) then
                iterationCount = 0
                call WriteToFile()
            endif

            if(MpiRank == 0) then
                stopCommand = NeedToStop(fileCount)
            endif
            
            call mpi_bcast(stopCommand, 1, MPI_LOGICAL, 0, MPI_COMM_WORLD, mpiError)
            call AssertMpiError(mpiError)
            if(stopCommand) then
                write(*,*) "Time to shutdown..."
#ifdef BACKUP_ENABLED
                write(*,*) "...and backup iteration..."
                call BackupIteration(iterationCount, fileCount)
#endif
                exit
            endif


#ifdef BACKUP_ENABLED
            backupCount = backupCount + 1
            if(mod(backupCount, BACKUP_STEP) == 0) then
                write(*,*) "Backup iteration..."
                call BackupIteration(iterationCount, fileCount)
                backupCount = 0
            endif
#endif
        enddo
        
        call MpiBarrier()
        call SPHDispose()
        call MpiFinalize()
        
        write(*,*) "Finish SPH"

    end subroutine
    
    subroutine WriteToFile()
        use :: SPH
        use :: MpiHelpers
        use :: DataWriter
        implicit none
        
        integer(4) :: mpiError, particleCount, globalParticleCount
        real(8) :: globalLuminosity_out
        character(6) :: stepchar
        character(2) :: rankchar
        
        if(MpiRank < 10) then
            write(rankchar,"(I1)") MpiRank
        else
            write(rankchar,"(I2)") MpiRank
        endif
        
        fileCount = fileCount + 1
        if(MpiRank == 0) then
            write(*,*) "Dumping file", fileCount
        endif
        write(stepchar,"(I6.6)") fileCount
        
        open(unit=1,file="data/"//trim(adjustl(rankchar))//"/result"//stepChar//".temp", status='replace')        
        call WriteData(1)
        close(1)
        
        particleCount = calculateParticles(0)
        call mpi_reduce(particleCount, globalParticleCount, 1, MPI_INTEGER, MPI_SUM, 0, MPI_COMM_WORLD, mpiError)
        call AssertMpiError(mpiError)
        
        call mpi_reduce(current_luminosity_out, globalLuminosity_out, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, mpiError)
        call AssertMpiError(mpiError)
        
        globalLuminosity_out = globalLuminosity_out * R_pl**2 * p_out**1.5d0 / rho_out**0.5d0 / dt1
        
        globalLuminosity_out = globalLuminosity_out *  (4.5d0 - (-3d0) )**2 * (3d0 - (-3d0)) 
        
        if(MpiRank == 0) then
            write(*,*) "Particles count: ", globalParticleCount
            write(*,*) "Luminosity: ", globalLuminosity_out
        endif

    end subroutine
end module

    
    
program SPHProgram
    use :: Main
    
    call Go()

end program