#include "SPHConfig.f90"

module SPH
    use :: Parameters
    use :: MpiHelpers
    use :: IntegerArray
    use :: IntegerPool
    use :: MpiTransferPoint
    use :: RealArray
    use :: Helpers
    use :: SPHKernel
    use :: SPHArea
    implicit none
    
    integer(4), parameter :: N = PARTICLES_COUNT
    real(8), parameter :: x_min1 = AREA_X_MIN
    real(8), parameter :: x_max1 = AREA_X_MAX
    real(8), parameter :: y_min1 = AREA_Y_MIN
    real(8), parameter :: y_max1 = AREA_Y_MAX
    
    real(8), parameter :: x_min = x_min1 * R_pl
    real(8), parameter :: x_max = x_max1 * R_pl
    real(8), parameter :: y_min = y_min1 * R_pl
    real(8), parameter :: y_max = y_max1 * R_pl

    real(8), parameter :: square = (x_max - x_min) * (y_max - y_min) !площадь свободного пространства
    real(8), parameter :: square1 = square / (R_pl**2)
    
    real(8), parameter :: h =  dsqrt(square / N) * 2d0
    real(8), parameter :: h1 = h / R_pl
    real(8), parameter :: hLimit1 = h1 * 2d0

    real(8), parameter :: m = rho_out * h**2 / 2d0 / 2d0
    real(8), parameter :: m1 = m / rho_out / (R_pl**2)
    
    integer(4), dimension(:), allocatable :: calculateParticlesGatherArray
    real(8), dimension(:), allocatable :: upperBorderScatterArray, bottomBorderScatterArray
    

#ifdef DYNAMIC_DT
    real(8) :: dt
    real(8) :: dt1
#else
    real(8), parameter :: dt = dsqrt(square / N) / V_pl / 5d0
    real(8), parameter :: dt1 = dt / R_pl * dsqrt(p_out / rho_out)
#endif

    type :: DataExchangeType
        type(IntegerArrayType) :: sendIndexes, recvIndexes
        type(IntegerArrayType) :: sendPointIndexes, recvPointIndexes
    endtype
    
    !поддержка кластерных участков
#ifdef USE_CLUSTER_MODE
    type(IntegerArrayType), dimension(:, :), allocatable, target :: PointClusterGrid
#endif    

    !данные
    type(PointType), dimension(:), allocatable :: point
    type(IntegerPoolType) :: freePointPool
    integer(4), dimension(:), pointer :: interactParticles, interactParticlesNext
    integer(4), dimension(:), allocatable, target :: interactParticlesBuff1, interactParticlesBuff2
    integer(4), dimension(:), pointer :: calculateParticles, calculateParticlesNext
    integer(4), dimension(:), allocatable, target :: calculateParticlesBuff1, calculateParticlesBuff2
    
    !Данные о границах
    real(8) :: upperBound1, bottomBound1, upperInteractionBound1, bottomInteractionBound1
    !MPI буфера
    type(DataExchangeType), target :: upperNodeData, bottomNodeData
    type(IntegerArrayType) :: foreignPointsIndexes

    type(IntegerArrayType) :: PointInteractionIndexes
        
    logical :: pointInteractionIndexesLoaded

    !исходящая светимость
    real(8) :: current_luminosity_out
    
    !количество итераций решения системы
    integer(4) :: solve_iteration_count_new, solve_iteration_count_old
    
    contains

    subroutine SwapActualParticlesArrays
        if(associated(interactParticles, interactParticlesBuff1)) then
            interactParticles => interactParticlesBuff2
            interactParticlesNext => interactParticlesBuff1
            calculateParticles => calculateParticlesBuff2
            calculateParticlesNext => calculateParticlesBuff1
        else
            interactParticles => interactParticlesBuff1
            interactParticlesNext => interactParticlesBuff2
            calculateParticles => calculateParticlesBuff1
            calculateParticlesNext => calculateParticlesBuff2
        endif
    end subroutine
    
    subroutine UpdateBorders()
        real(8) :: zoneSize
        integer(4) :: particleCount, i
        integer(4) :: mpiError
        real(8) :: endOfPrevArea, currentAreaSize
                
        call mpi_gather(calculateParticles(0), 1, MPI_INTEGER4, calculateParticlesGatherArray, 1, MPI_INTEGER4, 0, MPI_COMM_WORLD, mpiError)
        call AssertMpiError(mpiError)
        
        if(mpiRank == 0) then
            particleCount = sum(calculateParticlesGatherArray) * 1d0
            zoneSize = (y_max1 - y_min1)
            endOfPrevArea = y_max1
            
            do i = 1, MpiSize
                if(particleCount > 1000) then
                    currentAreaSize = zoneSize * (calculateParticlesGatherArray(i) * 1d0) / particleCount
                else
                    currentAreaSize = zoneSize / MpiSize
                endif
                upperBorderScatterArray(i) = endOfPrevArea
                bottomBorderScatterArray(i) = endOfPrevArea - currentAreaSize
                endOfPrevArea = bottomBorderScatterArray(i)
            enddo
        endif
        
        call mpi_scatter(upperBorderScatterArray, 1, MPI_REAL8, upperBound1, 1, MPI_REAL8, 0, MPI_COMM_WORLD, MpiError)
        call AssertMpiError(mpiError)
        call mpi_scatter(bottomBorderScatterArray, 1, MPI_REAL8, bottomBound1, 1, MPI_REAL8, 0, MPI_COMM_WORLD, MpiError)
        call AssertMpiError(mpiError)
        
        if(MpiRank == 0) then
            upperInteractionBound1 = upperBound1
        else
            upperInteractionBound1 = upperBound1 + hLimit1
        endif
        if(MpiRank == MpiSize-1) then
            bottomInteractionBound1 = bottomBound1
        else
            bottomInteractionBound1 = bottomBound1 - hLimit1
        endif
        
    end subroutine
    
    subroutine SPHInit()
        integer(4) :: ArrayN
        integer(4) :: i1, j1, i2, j2, fillerItem,  i
#ifdef USE_CLUSTER_MODE
        integer(4) :: j
#endif
        real(8) :: rankVerticalStep
        type(DataExchangeType), pointer :: targetExchangeData
        
        call SPHKernelInitialize(h1)
        call SPHAreaInitialize()
        
        pointInteractionIndexesLoaded = .false.

        rankVerticalStep = (y_max1 - y_min1) / MpiSize
        if(mpiRank == 0 .and. rankVerticalStep < h1 * 3d0) then
            call MpiAbortWithMessage("Too small h for this Mpi Size!")
        endif
        upperBound1 = y_max1 - rankVerticalStep * MpiRank
        bottomBound1 = upperBound1 - rankVerticalStep
        if(MpiRank == 0) then
            upperInteractionBound1 = upperBound1
        else
            upperInteractionBound1 = upperBound1 + hLimit1
        endif
        if(MpiRank == MpiSize-1) then
            bottomInteractionBound1 = bottomBound1
        else
            bottomInteractionBound1 = bottomBound1 - hLimit1
        endif
        
        ArrayN = N * 2
        
        !выделим массивы для Mpi (для начала меньше в 100 раз чем количество частиц)
        targetExchangeData => upperNodeData
        do i = 1, 2        
            targetExchangeData % sendIndexes % defaultAllocationSize = N / 100
            targetExchangeData % sendIndexes % expandMultiplyer = 2
        
            targetExchangeData % recvIndexes % defaultAllocationSize = N / 100
            targetExchangeData % recvIndexes % expandMultiplyer = 2
        
            targetExchangeData % recvPointIndexes % defaultAllocationSize = N / 100
            targetExchangeData % recvPointIndexes % expandMultiplyer = 2

            targetExchangeData % sendPointIndexes % defaultAllocationSize = N / 100
            targetExchangeData % sendPointIndexes % expandMultiplyer = 2

            targetExchangeData => bottomNodeData
        enddo
        
        !регистрация типов точек для Mpi
        call MpiRegisterTransferTypes()

#ifdef USE_CLUSTER_MODE
 !определим кластерную сеть
        i1 = floor(x_min1 / hLimit1)
        i2 = floor(x_max1 / hLimit1)
        j1 = floor(y_min1 / hLimit1)
        j2 = floor(y_max1 / hLimit1)
        
        !массивы поддержки кластеров
        allocate(PointClusterGrid(i1:i2,j1:j2))
        do i=i1, i2
            do j=j1, j2
                PointClusterGrid(i, j) % defaultAllocationSize = 50
                PointClusterGrid(i, j) % expandMultiplyer = 2
                PointClusterGrid(i, j) % Cursor = 0
            enddo
        enddo
#endif

        allocate(point(0:ArrayN)) !должен начинаться от нуля (для индексов MPI)

        !заполняем пул номеров точек
        fillerItem = 0
        call freePointPool % FillPoolElements(PoolFiller, ArrayN)
        
        foreignPointsIndexes % defaultAllocationSize = N / 100
        foreignPointsIndexes % expandMultiplyer = 2
        
        PointInteractionIndexes % defaultAllocationSize = N / 100
        PointInteractionIndexes % expandMultiplyer = 2
    
        allocate(interactParticlesBuff1(0:ArrayN))
        allocate(interactParticlesBuff2(0:ArrayN))
        allocate(calculateParticlesBuff1(0:ArrayN))
        allocate(calculateParticlesBuff2(0:ArrayN))
        
        !инициализация указателей
        call SwapActualParticlesArrays()

        interactParticlesBuff1 = 0
        interactParticlesBuff2 = 0
        calculateParticlesBuff1 = 0
        calculateParticlesBuff2 = 0

        allocate(calculateParticlesGatherArray(mpiSize))
        allocate(upperBorderScatterArray(mpiSize))
        allocate(bottomBorderScatterArray(mpiSize))
        call UpdateBorders()
        
#ifdef DYNAMIC_DT
        !начальные значения для dt
        dt = dsqrt(square / N) / V_pl / 5d0
        dt1 = dt / R_pl * dsqrt(p_out / rho_out)
#endif

    contains
        function PoolFiller() result(res) !должен перечислить от нуля (для индексов MPI)
            integer(4) :: res
            res = fillerItem
            fillerItem = fillerItem + 1
        end function
    end subroutine
    
    subroutine SPHDispose
        integer(4) :: i
#ifdef USE_CLUSTER_MODE
        integer(4) :: j
#endif
        type(DataExchangeType), pointer :: targetExchangeData

        !освобождение массивов Mpi
        targetExchangeData => upperNodeData
        do i = 1, 2
            call targetExchangeData % sendIndexes % Dispose()
            call targetExchangeData % recvIndexes % Dispose()
            call targetExchangeData % recvPointIndexes % Dispose()
            call targetExchangeData % sendPointIndexes % Dispose()
            targetExchangeData => bottomNodeData
        enddo
        
        
        call MpiUnregisterTransferTypes()
        
#ifdef USE_CLUSTER_MODE
        !массивы кластера
        do i = lbound(PointClusterGrid, 1), ubound(PointClusterGrid, 1)
            do j = lbound(PointClusterGrid, 2), ubound(PointClusterGrid, 2)
                call PointClusterGrid(i,j) % Dispose()
            enddo
        enddo
        deallocate(PointClusterGrid)
    
        !освобождаем поддержку кластеризации
        call PointInteractionIndexes % Dispose()
#endif
        
        deallocate(point)
        !освободить пул номеров точек
        call freePointPool % Dispose()
        
        call foreignPointsIndexes % Dispose()
        
        deallocate(interactParticlesBuff1)
        deallocate(interactParticlesBuff2)
        deallocate(calculateParticlesBuff1)
        deallocate(calculateParticlesBuff2)
        
        deallocate(calculateParticlesGatherArray)
        deallocate(upperBorderScatterArray)
        deallocate(bottomBorderScatterArray)
    end subroutine
    
    
#ifdef CALCULATE_PARTICLE_INTERACTION_COUNT
    subroutine UpdateInteractionCount()
        integer(4) :: b, bCursor, a, aCursor
    
        do aCursor=1, calculateParticles(0)
            a = calculateParticles(aCursor)

            point(a) % interactionCount = 0

            call LoadPointInteractionIndexes(a)
            do bCursor = 1, PointInteractionIndexes % Cursor
                b = PointInteractionIndexes % Instance(bCursor)
                
                point(a) % interactionCount = point(a) % interactionCount + 1
            enddo 
            call RelaxPointInteractionIndexes()
        enddo
    end subroutine
#endif

    subroutine UpdateRho()
        integer(4) :: b, bCursor, a, aCursor
        real(8) :: currentW_ab, dist


        do aCursor=1, calculateParticles(0)
            a = calculateParticles(aCursor)

            point(a) % rho = 0d0
            
            call LoadPointInteractionIndexes(a)
            do bCursor = 1, PointInteractionIndexes % Cursor
                b = PointInteractionIndexes % Instance(bCursor)
                
                currentW_ab = GetW(point(a) % r - point(b) % r)
                point(a) % rho = point(a) % rho + currentW_ab
            enddo
            call RelaxPointInteractionIndexes()

            point(a) % rho = point(a) % rho * m1

            !домножим на фактор планеты
            dist = VecNorm(point(a) % r)
            point(a) % rho = point(a) % rho * GetBodyIntersectCorrection(dist, h1, hLimit1)
        enddo
    end subroutine

    function GetGradient(a_index, value_getter) result(gradient)

        integer(4), intent(in) :: a_index
        real(8), dimension(2) :: gradient
        interface
            function value_getter(value_getter_index) result(value_getter_result)
                integer(4), intent(in) :: value_getter_index
                real(8) :: value_getter_result
            end function
        end interface
        real(8) :: aValue, bValue
        real(8), dimension(2) :: a_r
        integer(4) :: b_index, bCursor

        real(8), dimension(2) :: currentGradW_ab

        gradient = 0d0
        aValue = value_getter(a_index)
        a_r = point(a_index) % r
        
        call LoadPointInteractionIndexes(a_index)
        do bCursor = 1, PointInteractionIndexes % Cursor
            b_index = PointInteractionIndexes % Instance(bCursor)
            
            if(a_index == b_index) then
                cycle
            endif
            
            bValue = value_getter(b_index)
            call GetGradW(a_r - point(b_index) % r, gradW = currentGradW_ab)
            gradient = gradient + (bValue - aValue) * currentGradW_ab
        enddo
        call RelaxPointInteractionIndexes()

        gradient = gradient * m1 / point(a_index) % rho
    end function
    
    subroutine GetDiverge(a_index, value_getter, diverge, divergeComponents)
        integer(4), intent(in) :: a_index
        real(8), optional :: diverge
        real(8), dimension(2,2), optional :: divergeComponents
        interface
            function value_getter(value_getter_index) result(value_getter_result)
                integer(4), intent(in) :: value_getter_index
                real(8), dimension(2) :: value_getter_result
            end function
        end interface
        real(8), dimension(2) :: aValue, bValue, valueBminusValueA
        integer(4) :: b_index, bCursor
        real(8) :: dist, planetFactor

        real(8), dimension(2) :: currentGradW_ab, a_r

        if(present(diverge)) then
            diverge = 0d0
        endif
        if(present(divergeComponents)) then
            divergeComponents = 0d0
        endif

        aValue = value_getter(a_index)
        a_r = point(a_index) % r
        
        call LoadPointInteractionIndexes(a_index)
        do bCursor = 1, PointInteractionIndexes % Cursor
            b_index = PointInteractionIndexes % Instance(bCursor)
            
            if(a_index == b_index) then
                cycle
            endif
            
            bValue = value_getter(b_index)
            call GetGradW(a_r - point(b_index) % r, gradW = currentGradW_ab)
            
            valueBminusValueA = bValue - aValue
            
            if(present(diverge)) then
                diverge = diverge + dot_product(valueBminusValueA, currentGradW_ab)
            endif
            if(present(divergeComponents)) then
                divergeComponents(1,1) = divergeComponents(1,1) + valueBminusValueA(1) * currentGradW_ab(1)
                divergeComponents(1,2) = divergeComponents(1,2) + valueBminusValueA(1) * currentGradW_ab(2)
                
                divergeComponents(2,1) = divergeComponents(2,1) + valueBminusValueA(2) * currentGradW_ab(1)
                divergeComponents(2,2) = divergeComponents(2,2) + valueBminusValueA(2) * currentGradW_ab(2)
            endif

        enddo
        call RelaxPointInteractionIndexes()

        !домножим на фактор планеты
        dist = VecNorm(a_r)
        planetFactor = 1d0 !GetBodyIntersectCorrection(dist, h1, hLimit1) !временно отменена поправка на планету в дивергенции
        
        if(present(diverge)) then
            diverge = diverge * m1 / point(a_index) % rho
            diverge = diverge * planetFactor
        endif
        
        if(present(divergeComponents)) then
            divergeComponents = divergeComponents * m1 / point(a_index) % rho
            divergeComponents = divergeComponents * planetFactor
        endif    
    end subroutine

    function GetLaplace(a_index, value_getter) result(laplacian)
        integer(4), intent(in) :: a_index
        real(8) :: laplacian
        interface
            function value_getter(value_getter_index) result(value_getter_result)
                integer(4), intent(in) :: value_getter_index
                real(8) :: value_getter_result
            end function
        end interface
        real(8) :: aValue, bValue
        integer(4) :: b_index, bCursor
        real(8) :: currentLaplaceW_ab
        real(8), dimension(2) :: a_r

        laplacian = 0d0
        aValue = value_getter(a_index)
        a_r = point(a_index) % r
        
        call LoadPointInteractionIndexes(a_index)
        do bCursor = 1, PointInteractionIndexes % Cursor
            b_index = PointInteractionIndexes % Instance(bCursor)
            
            if(a_index == b_index) then
                cycle
            endif
            
            bValue = value_getter(b_index)
            currentLaplaceW_ab = GetLaplaceW(a_r - point(b_index) % r)
            laplacian = laplacian + (bValue - aValue) * currentLaplaceW_ab
        enddo
        call RelaxPointInteractionIndexes()

        laplacian = laplacian * m1 / point(a_index) % rho

        !домножим на фактор планеты
        !dist = VecNorm(a_r)
        !laplacian = laplacian * GetBodyIntersectCorrection(dist, h1, hLimit1)
    end function
    
    subroutine LoadPointInteractionIndexes(a)
        integer(4), intent(in) :: a
        real(8) :: dist
        real(8), dimension(2) :: ra_rb
        integer(4) :: b, bCursor
#ifdef USE_CLUSTER_MODE
        integer(4) :: i1, j1, i2, j2, i, j, visitedCount
        type(IntegerArrayType), pointer :: currentCell
#endif

        if(pointInteractionIndexesLoaded) then
            call MpiAbortWithMessage("Double call LoadPointInteractionIndexes!")
        endif

        PointInteractionIndexes % Cursor = 0
#ifdef USE_CLUSTER_MODE
        call GetPointBlockCoordinates(point(a) % r(1), point(a) % r(2), i1, j1, i2, j2)
        visitedCount = 0
        
        do i = i1, i2    
            do j = j1, j2
                currentCell => PointClusterGrid(i, j) !обычное присвоение вызовет копирование => утечку памяти
                do bCursor = 1, currentCell % Cursor
                    b = currentCell % Instance(bCursor)

                    !здесь начинается код метода
                    ra_rb = point(a) % r - point(b) % r
                    dist = VecNorm(ra_rb)
                    if(dist <= hLimit1) then
                        call PointInteractionIndexes % SafeAddItem(b)
                    endif
                enddo
            enddo
        enddo
#else

        do bCursor=1, interactParticles(0)
            b = interactParticles(bCursor)   
        
            !здесь начинается код метода
            ra_rb = point(a) % r - point(b) % r
            dist = VecNorm(ra_rb)
            if(dist <= hLimit1) then
                call PointInteractionIndexes % SafeAddItem(b)
            endif
        enddo
#endif
        pointInteractionIndexesLoaded = .true.
    end subroutine
    
    subroutine RelaxPointInteractionIndexes()
    implicit none
        pointInteractionIndexesLoaded = .false.
    end subroutine

#ifdef USE_CLUSTER_MODE
    subroutine GetPointCellCoordinates(x, y, i, j)
        real(8), intent(in) :: x, y
        integer(4), intent(out) :: i, j
        i = floor(x / hLimit1)
        j = floor(y / hLimit1)
    end subroutine
    
    subroutine GetPointBlockCoordinates(x, y, i1, j1, i2, j2)
        real(8), intent(in) :: x,y
        integer(4), intent(out) :: i1, j1, i2, j2
        integer(4) :: i, j
        
        call GetPointCellCoordinates(x, y, i, j)

        !квадрат 3x3
        i1 = i - 1
        j1 = j - 1
        i2 = i + 1
        j2 = j + 1
        !обрезаем по границам грида
        i1 = max(i1, lbound(PointClusterGrid, 1))
        j1 = max(j1, lbound(PointClusterGrid, 2))
        i2 = min(i2, ubound(PointClusterGrid, 1))
        j2 = min(j2, ubound(PointClusterGrid, 2))
    end subroutine
    
    subroutine UpdateClusterGrid()
        integer(4) :: a
        integer(4) :: aCursor
        integer(4) :: i, j

        do i = lbound(PointClusterGrid, 1), ubound(PointClusterGrid, 1) 
            do j = lbound(PointClusterGrid, 2), ubound(PointClusterGrid, 2) 
                PointClusterGrid(i, j) % Cursor = 0
            enddo
        enddo

        do aCursor=1, interactParticles(0)
            a = interactParticles(aCursor)
            call GetPointCellCoordinates(point(a) % r(1), point(a) % r(2), i, j)
            call PointClusterGrid(i, j) % SafeAddItem(a)
        enddo
    end subroutine
#endif

    subroutine UpdateEnergy(i)
    use :: Polynomial234RootSolvers, only : quarticRoots
    implicit none
        integer(4), intent(in) :: i
        real(8) :: D_n_i, P_n_i, a0, a0_norm, a1_norm, b_ij !обозначения из Whitehouse 2005
        integer(4) :: j, jCursor
        real(8), dimension(2) :: currentGradW_ij
        real(8) :: currentGradW_ij_divR, visc_ij
        real(8), dimension(2) :: r_ij, v_ij
        
        real(8), dimension(4, 2) :: roots
        integer(4) :: nReal

        D_n_i = 0d0
        P_n_i = 0d0
        
        call LoadPointInteractionIndexes(i)
        do jCursor = 1, PointInteractionIndexes % Cursor
            j = PointInteractionIndexes % Instance(jCursor)
            
            if(i == j) then
                cycle
            endif
            
            r_ij = point(i) % r - point(j) % r
            v_ij = point(i) % v - point(j) % v
            
            call GetGradW(r_ij, gradW = currentGradW_ij, gradW_devided_By_R = currentGradW_ij_divR)
            
            b_ij = 4d0 * point(i) % lambdaDivKappaRho * point(j) % lambdaDivKappaRho / (point(i) % lambdaDivKappaRho + point(j) % lambdaDivKappaRho)
            
            visc_ij = GetViscosity(r_ij, v_ij, point(i) % rho, point(j) % rho, point(i) % p, point(j) % p)
            
            D_n_i = D_n_i - m1 / point(i) % rho * c_const1 * b_ij * currentGradW_ij_divR * point(j) % ksiNext
            P_n_i = P_n_i + 0.5d0 * m1 * dot_product(v_ij, currentGradW_ij) * ((Poisson_const - 1d0) * point(j) % uNext / point(j) % rho + visc_ij)
        enddo
        call RelaxPointInteractionIndexes()
        
        a0 = point(i) % beta * point(i) % ksi + (point(i) % hi - point(i) % beta - 1d0) * (-point(i) % u - dt1 * P_n_i) + dt1 * D_n_i * point(i) % beta
        
        a1_norm = point(i) % a1 / point(i) % a4
        a0_norm = a0 / point(i) % a4
        
        call quarticRoots (0d0, 0d0, a1_norm, a0_norm, nReal, roots)
        if(nReal == 0) then
            call MpiAbortWithMessage("No real roots due solving quartic poly!")
        endif
        
        point(i) % uNextNew = SelectEnergyRoot(roots, point(i) % T_gas, point(i) % T_rad)
        point(i) % ksiNextNew = (point(i) % ksi + dt1 * D_n_i + dt1 * point(i) % Gamma * (point(i) % uNextNew**4)) / (1d0 - point(i) % hi + point(i) % beta)

    contains
    
    
        function SelectEnergyRoot(selectEnergyRoot_roots, T_gas, T_rad) result(selectEnergyRoot_result)
        implicit none
            real(8), dimension(4, 2), intent(in) :: selectEnergyRoot_roots
            real(8), intent(in) :: T_gas, T_rad
            real(8) :: T_root, T_left, T_right
            real(8) :: selectEnergyRoot_result, distance
            logical :: found
            
            if(T_gas < T_rad) then
                T_left = T_gas
                T_right = T_rad
            else
                T_left = T_rad
                T_right = T_gas              
            endif
            
            do j = 1, 4
                if(roots(j, 2) == 0d0 .and. roots(j, 1) >= 0d0) then
                    T_root = roots(j, 1) / c_v_i1
                    
                    if(T_root >= T_left .and. T_root <= T_right) then
                        selectEnergyRoot_result = roots(j, 1)
                        return
                    endif
                endif
            enddo
            
            distance = huge(0d0)
            found = .false.
            
            do j = 1, 4
                if(roots(j, 2) == 0d0 .and. roots(j, 1) >= 0d0) then
                    !T_root = roots(j, 1) / c_v_i1
                    found = .true.
                    distance = min(distance, roots(j, 1))
                    selectEnergyRoot_result = distance
                    !if(dabs(T_gas - T_root) < distance) then
                    !    distance = dabs(T_gas - T_root)
                    !    selectEnergyRoot_result = roots(j, 1)
                    !    found = .true.
                    !endif
                endif
            enddo
            
            if(.not.found) then
                call MpiAbortWithMessage("Correct root was not found!")
            endif
        end function
    
    end subroutine

    function GetVelocity(getVelocity_index) result(getVelocity_result)
    implicit none
        integer(4), intent(in) :: getVelocity_index
        real(8), dimension(2) :: getVelocity_result

        getVelocity_result = point(getVelocity_index) % v
    end function

    
    subroutine LoadEnergyIterationIndependentCoefficients(i)
    implicit none
        integer(4), intent(in) :: i
        real(8) :: R_p_i, b_ij !обозначения из Whitehouse 2005
        real(8) :: R_i, k_i, D_d_i, P_d_i, lambda_i
        real(8), parameter :: a = 4d0 * sigma_const1 / c_const1
        integer(4) :: j, jCursor
        real(8), dimension(2) :: currentGradW_ij
        real(8) :: currentGradW_ij_divR, f
        real(8), dimension(2) :: r_ij, v_ij
    
        k_i = point(i) % Kappa
        R_i = point(i) % R_coefficient
        lambda_i = point(i) % Lambda
    
        point(i) % beta = dt1 * c_const1 * k_i * point(i) % rho
        point(i) % Gamma = a * c_const1 * k_i / c_v_i1**4 
        
        D_d_i = 0d0
        P_d_i = 0d0
        
        call LoadPointInteractionIndexes(i)
        do jCursor = 1, PointInteractionIndexes % Cursor
            j = PointInteractionIndexes % Instance(jCursor)
            
            if(i == j) then
                cycle
            endif
            
            r_ij = point(i) % r - point(j) % r
            v_ij = point(i) % v - point(j) % v
            
            call GetGradW(r_ij, gradW = currentGradW_ij, gradW_devided_By_R = currentGradW_ij_divR)
            
            b_ij = 4d0 * point(i) % lambdaDivKappaRho * point(j) % lambdaDivKappaRho / (point(i) % lambdaDivKappaRho + point(j) % lambdaDivKappaRho)
            
            D_d_i = D_d_i + 1d0 / point(j) % rho * b_ij * currentGradW_ij_divR
            P_d_i = P_d_i + dot_product(v_ij, currentGradW_ij) / point(i) % rho
        enddo
        call RelaxPointInteractionIndexes()
        
        D_d_i = D_d_i * m1 * c_const1
        P_d_i = P_d_i * 0.5d0 * m1 * (Poisson_const - 1d0)
        
        f = getF(lambda_i, R_i)
        R_p_i = GetContractionPV(i, f)
        point(i) % hi = dt1 * (D_d_i - R_p_i)
        
        point(i) % a4 = point(i) % Gamma * dt1 * (point(i) % hi - 1d0)
        point(i) % a1 = (point(i) % hi - point(i) % beta - 1d0) * (1d0 - dt1 * P_d_i)
    contains
    
        function GetContractionPV(a, f) result(contraction)
            real(8) :: contraction
            real(8), intent(in) :: f
            integer(4), intent(in) :: a
            real(8), dimension(2,2) :: divVComponents
            real(8) :: mixedDivergeV, divergeV, first_component, second_component
            real(8), dimension(2) :: gradKsi, normGradKsi
            
            call GetDiverge(a, GetVelocity, divergeComponents = divVComponents)
            
            gradKsi = GetGradient(a, GetKsi)
            
            if(normGradKsi(1) /= 0d0 .or. normGradKsi(2) /= 0d0) then
                normGradKsi = gradKsi / VecNorm(gradKsi)
            else
                normGradKsi = 0d0
            endif
            
            first_component = 0.5d0 * (1d0 - f)
            
            second_component = 0.5d0 * (3d0 * f - 1d0)
            
            contraction = (first_component + second_component * normGradKsi(1)**2) * divVComponents(1,1)
            contraction = contraction + second_component * normGradKsi(1) * normGradKsi(2) * divVComponents(1,2)
            contraction = contraction + second_component * normGradKsi(1) * normGradKsi(2) * divVComponents(2,1)
            contraction = contraction + (first_component + second_component * normGradKsi(2)**2) * divVComponents(2,2)
            
        end function
        
        function GetKsi(getKsi_index) result(getKsi_result)
        implicit none
            integer(4), intent(in) :: getKsi_index
            real(8)  :: getKsi_result

            getKsi_result = point(getKsi_index) % ksi
        end function
        
        function GetVelocity(getVelocity_index) result(getVelocity_result)
        implicit none
            integer(4), intent(in) :: getVelocity_index
            real(8), dimension(2) :: getVelocity_result

            getVelocity_result = point(getVelocity_index) % v
        end function
    end subroutine

    
    function GetF(getF_Lambda, getF_R) result(getF_result)
    implicit none
        real(8), intent(in) :: getF_Lambda, getF_R
        real(8) :: getF_result
        real(8), parameter :: R_Limit = 1d5
            
        if(getF_R < R_Limit) then
            getF_result = getF_Lambda + (getF_Lambda**2) * (getF_R**2)
        else
            getF_result = getF_Lambda + 1d0
        endif
    end function
    
    function GetLambda(getLambda_R) result(getLambda_result)
    implicit none
        real(8), intent(in) :: getLambda_R
        real(8) :: getLambda_result
        real(8), parameter :: R_Limit = 1d5
            
        if(getLambda_R < R_Limit) then
            getLambda_result = (2d0 + getLambda_R) / (6d0 + 3d0 * getLambda_R + getLambda_R * getLambda_R)
        else
            getLambda_result = 1d0 / getLambda_R
        endif
    end function
    
    function GetR(i) result(R)
    implicit none
        integer(4), intent(in) :: i
        real(8) :: R
        real(8), dimension(2) :: energyGradient

        energyGradient = GetGradient(i, KsiRhoGetter)
        R = VecNorm(energyGradient) / (point(i) % Kappa * point(i) % rho * KsiRhoGetter(i))

    contains
        function KsiRhoGetter(energyGetter_Index) result(energyGetter_result)
        implicit none
            integer(4), intent(in) :: energyGetter_Index
            real(8) :: energyGetter_result
            energyGetter_result = point(energyGetter_Index) % ksi * point(energyGetter_Index) % rho
        end function
    end function
    
    !обновляет энергии своих и (!) удаленных частиц соседних нод
    subroutine UpdateEnergies()
    use :: RosselandMean
    implicit none
        integer(4) :: a, aCursor, mpiError
        real(8) :: ksiOld, uOld, iterationDiffLocal, iterationDiffGlobal, rosseland
        real(8) :: u_avg_local, u_avg_global, u_coeff, ksi_coeff, u_old, ksi_old
        integer(4) :: u_avg_count_local, u_avg_count_global

        do aCursor=1, calculateParticles(0)
            a = calculateParticles(aCursor)
            point(a) % uNext = point(a) % u
            point(a) % ksiNext = point(a) % ksi

            rosseland = GetRosselandMean(point(a) % rho, point(a) % T_gas, ROSSELAND_MEAN_LIMIT, point(a) % rosseland_was_cutted)
            
            point(a) % Kappa = 1d0 / rosseland / point(a) % rho
            point(a) % R_coefficient = GetR(a)
            point(a) % Lambda = GetLambda(point(a) % R_coefficient)
            point(a) % lambdaDivKappaRho = point(a) % Lambda * rosseland
        enddo

        !передадим и примем lambdaDivKappaRho
        call MpiExchangeTwoIndexedSubArrays( &
            targetArray = point, &
            upperSendIndexes = upperNodeData % sendIndexes, &
            bottomSendIndexes = bottomNodeData % sendIndexes, &
            upperRecvIndexes = upperNodeData % recvIndexes, &
            bottomRecvIndexes = bottomNodeData % recvIndexes, &
            MpiType = MPI_TYPE_TransferPointLambdaDivKappaRho)

        do aCursor=1, calculateParticles(0)
            a = calculateParticles(aCursor)
            call LoadEnergyIterationIndependentCoefficients(a)
        enddo
        
        do aCursor = 1, foreignPointsIndexes % Cursor
            a = foreignPointsIndexes % Instance(aCursor)
            point(a) % uNext = point(a) % u
            point(a) % ksiNext = point(a) % ksi
        enddo  
        
        solve_iteration_count_old = solve_iteration_count_new
        solve_iteration_count_new = 0
        
        do
            iterationDiffLocal = -huge(0d0)

            do aCursor=1, calculateParticles(0)
                a = calculateParticles(aCursor)

                ksiOld = point(a) % ksiNext
                uOld = point(a) % uNext             

                call UpdateEnergy(a)
            
                iterationDiffLocal = max(dabs(uOld - point(a) % uNextNew) / uOld, iterationDiffLocal)
                iterationDiffLocal = max(dabs(ksiOld - point(a) % ksiNextNew) / ksiOld, iterationDiffLocal)
            enddo
            
            do aCursor=1, calculateParticles(0)
                a = calculateParticles(aCursor)

                point(a) % ksiNext = point(a) % ksiNextNew
                point(a) % uNext = point(a) % uNextNew
            enddo

            call mpi_reduce(iterationDiffLocal, iterationDiffGlobal, 1, MPI_REAL8, MPI_MAX, 0, MPI_COMM_WORLD, mpiError)
            call AssertMpiError(mpiError)
            call mpi_bcast(iterationDiffGlobal, 1, MPI_REAL8, 0, MPI_COMM_WORLD, mpiError)
            call AssertMpiError(mpiError)
            
            !передадим и примем ksiNext и uNext
            call MpiExchangeTwoIndexedSubArrays( &
                targetArray = point, &
                upperSendIndexes = upperNodeData % sendIndexes, &
                bottomSendIndexes = bottomNodeData % sendIndexes, &
                upperRecvIndexes = upperNodeData % recvIndexes, &
                bottomRecvIndexes = bottomNodeData % recvIndexes, &
                MpiType = MPI_TYPE_TransferPointUNextKsiNext)
            
            solve_iteration_count_new = solve_iteration_count_new + 1
            
            if(iterationDiffGlobal < INTERATION_PRECISE) then
                exit
            endif
        enddo
        
        !закрепим u и ksi и найдем минимум энергии
        u_avg_local = 0d0
        u_avg_count_local = 0
        do aCursor=1, calculateParticles(0)
            a = calculateParticles(aCursor)
            
            !!!
            point(a) % a1 = point(a) % u
            point(a) % a4 = point(a) % ksi
            !!!
            
            point(a) % u = point(a) % uNext
            point(a) % ksi = point(a) % ksiNext
            
            !if(point(a) % u < u_out1) then
            !    point(a) % u = u_out1
            !endif
            !if(point(a) % ksi < ksi_out1) then
            !    point(a) % ksi = ksi_out1
            !endif
            
            !u_avg_count_local, u_avg_count_global
            if( point(a) % r(1) >= X_MIN1 .and. point(a) % r(1) <= X_MIN1 + 1d0 .and. abs(point(a) % r(2)) < Y_MAX1 .and. abs(point(a) % r(2)) > Y_MAX1 - 1d0 ) then
                u_avg_local = u_avg_local + point(a) % u
                u_avg_count_local = u_avg_count_local + 1
            endif

        enddo 
        
        !среднее энергии в кластере в "тихой" зоне
        call mpi_reduce(u_avg_local, u_avg_global, 1, MPI_REAL8, MPI_SUM, 0, MPI_COMM_WORLD, mpiError)
        call AssertMpiError(mpiError)
        call mpi_reduce(u_avg_count_local, u_avg_count_global, 1, MPI_INTEGER4, MPI_SUM, 0, MPI_COMM_WORLD, mpiError)
        call AssertMpiError(mpiError)
        
        if(mpiRank == 0) then
            u_avg_global = u_avg_global / real(u_avg_count_global)
        endif
        
        call mpi_bcast(u_avg_global, 1, MPI_REAL8, 0, MPI_COMM_WORLD, mpiError)
        call AssertMpiError(mpiError)
        
        current_luminosity_out = 0d0
        if(u_avg_global > u_out1) then
        
            u_coeff = u_out1 / u_avg_global
            ksi_coeff = ksi_out1 / ( 4d0 * sigma_const1 / (c_const1 * rho_out1) * (u_avg_global / c_v_i1)**4 ) !4d0 * sigma_const1 / c_const1 / (c_v_i1**4)
        
            do aCursor=1, calculateParticles(0)
                a = calculateParticles(aCursor)
            
                u_old = point(a) % u
                ksi_old = point(a) % ksi
                
                point(a) % u = point(a) % u * u_coeff
                point(a) % ksi = point(a) % ksi * ksi_coeff !/ point(a) % rho * (point(a) % u**4)
                
                !if(point(a) % u < u_out1) then
                !    point(a) % u = u_out1
                !endif
                !if(point(a) % ksi < ksi_out1) then
                !    point(a) % ksi = ksi_out1
                !endif
                
                if(point(a) % r(1) >= LUMINOSITY_WINDOW_X_MIN .and. point(a) % r(1) <= LUMINOSITY_WINDOW_X_MAX) then
                    if(point(a) % r(2) >= LUMINOSITY_WINDOW_Y_MIN .and. point(a) % r(2) <= LUMINOSITY_WINDOW_Y_MAX) then
                        current_luminosity_out = current_luminosity_out + (( u_old - point(a) % u ) + ( ksi_old - point(a) % ksi )) * point(a) % rho
                    endif
                endif
            enddo
        endif
        
    end subroutine
    
    subroutine UpdateDV(a)
    implicit none
        integer(4), intent(in) :: a
        real(8) :: visc_ab
        real(8) :: PbaRhoaPbRhob
        real(8), dimension(2) :: gradW_ab, r_ab, v_ab, gasAccel, radiationAccel, gravityAccel
        integer(4) :: bCursor, b

        gasAccel = 0d0
        radiationAccel = 0d0
            
        call LoadPointInteractionIndexes(a)
        do bCursor = 1, PointInteractionIndexes % Cursor
            b = PointInteractionIndexes % Instance(bCursor)
            
            if(a == b) then
                cycle
            endif
        
            r_ab = point(a) % r - point(b) % r
            v_ab = point(a) % v - point(b) % v
        
            PbaRhoaPbRhob = point(a) % p / (point(a) % rho**2) + point(b) % p / (point(b) % rho**2)
            visc_ab = GetViscosity(r_ab, v_ab, point(a) % rho, point(b) % rho, point(a) % p, point(b) % p)
                
            call GetGradW(r_ab, gradW = gradW_ab)
                
            !вынесем m1 за скобку!!!
            gasAccel = gasAccel - (PbaRhoaPbRhob + visc_ab) * gradW_ab
            !вынесем lambda/rho*m за скобку
            radiationAccel = radiationAccel - point(b) % ksi * gradW_ab
        enddo
        call RelaxPointInteractionIndexes()
        
        gasAccel = gasAccel * m1
        radiationAccel = radiationAccel * m1 * point(a) % Lambda / point(a) % rho
        gravityAccel = GravityAccelerationDV(point(a) % r)
        
        point(a) % dv = gasAccel + radiationAccel + gravityAccel

    end subroutine
    
    subroutine SPHIteration
    implicit none
        integer(4) :: a
        integer(4) :: aCursor
        
        !обновление положения частиц, трансфер частиц, подсчет новых плотности и давления
        call UpdatePointParameters()

        !высчитываем дивергенцию скорости
        do aCursor=1, calculateParticles(0)
            a = calculateParticles(aCursor)
            call GetDiverge(a, GetVelocity, diverge = point(a) % divV)
        enddo
        
#ifdef CALCULATE_PARTICLE_INTERACTION_COUNT
        call UpdateInteractionCount()
#endif

#ifdef DYNAMIC_DT
        call UpdateDt()
#endif

        call UpdateEnergies()
        
        do aCursor=1, calculateParticles(0)
            a = calculateParticles(aCursor)
            call UpdateDV(a)                
        enddo
            
        !пересчитаем энергию и параметры используя dt1
        do aCursor=1, calculateParticles(0)
            a = calculateParticles(aCursor)

            point(a) % dr = point(a) % v * dt1 + point(a) % dv * dt1**2 / 2d0
            point(a) % v = point(a) % v + point(a) % dv * dt1 
            !
            !if(point(a) % u < 0d0 .or. point(a) % ksi < 0d0) then
            !    write(*,*) "u < 0 or ksi < 0", point(a) % u, point(a) % ksi, point(a) % r(1), point(a) % r(2)
            !    point(a) % u = u_out1
            !    point(a) % ksi = ksi_out1
            !endif
            
            !для дебага, должна быть переписана в UpdatePointParameters
#ifdef DEBUG
            point(a) % p = -1d0
            point(a) % rho = -1d0
            point(a) % T_gas = -1d0
            point(a) % T_rad = -1d0
#endif            
        enddo
        
    end subroutine
    
#ifdef DYNAMIC_DT
    subroutine UpdateDt()
        real(8), parameter :: tauGamma = 0.3d0
        real(8), parameter :: alpha = 1d0
        real(8), parameter :: beta = 2d0
        real(8) :: currentDevider, C, V, maxDevider
        real(8) :: localTauMin1, globalTauMin1, localTauMin1_1, localTauMin1_2, system_solve_coefficient
        integer(4) :: i, mpiError, a
        
        localTauMin1_1 = huge(0d0) !нода без частиц не должна учавствовать в выборе tau
        if (calculateParticles(0) /= 0) then
            maxDevider = -huge(0d0)
            do i=1, calculateParticles(0)
                a = calculateParticles(i)
            
                C = GetSpeedOfSound(point(a) % rho, point(a) % p)
                V = VecNorm(point(a) % v)
                currentDevider = C + h1 * dabs(point(a) % divV) + 1.2d0 * (alpha * C + beta * h1 * dabs(point(a) % divV))
            
                maxDevider = max(currentDevider, maxDevider)
            enddo
            localTauMin1_1 = tauGamma * h1 / maxDevider
        endif
        
        localTauMin1_2 = huge(0d0)
        if (calculateParticles(0) /= 0) then
            maxDevider = -huge(0d0)
            do i=1, calculateParticles(0)
                a = calculateParticles(i)

                currentDevider = VecNorm(point(a) % dv)
                
                if(currentDevider /= 0d0) then
                    maxDevider = max(currentDevider, maxDevider)
                endif
            enddo
            if(maxDevider /= -huge(0d0)) then
                localTauMin1_2 = tauGamma * dsqrt(h1 / maxDevider)
            endif
        endif
        
        localTauMin1 = min(localTauMin1_1, localTauMin1_2)
        
        if(solve_iteration_count_old /= 0) then
            system_solve_coefficient = real(solve_iteration_count_old) / real(solve_iteration_count_new)
            if(system_solve_coefficient < 1d0) then
                localTauMin1 = localTauMin1 * system_solve_coefficient
            endif
        endif
        
        call mpi_reduce(localTauMin1, globalTauMin1, 1, MPI_REAL8, MPI_MIN, 0, MPI_COMM_WORLD, mpiError)
        call AssertMpiError(mpiError)
        
        if(globalTauMin1 == huge(0d0)) then !частиц вообще небыло в зоне, поставим стандартные значения
            globalTauMin1 = (dsqrt(square / N) / V_pl) / R_pl * dsqrt(p_out / rho_out)
        endif

        call mpi_bcast(globalTauMin1, 1, MPI_REAL8, 0, MPI_COMM_WORLD, mpiError)
        call AssertMpiError(mpiError)
        
        dt1 = globalTauMin1
        dt = dt1 * R_pl / dsqrt(p_out / rho_out)
    end subroutine
#endif
    
    function IsActualParticle(x, y) result(res)
        real(8) :: x, y
        logical :: res
        res = x <= x_max1 .and. x >= x_min1 .and. y <= y_max1 .and. y >= y_min1
    end function
    
    function IsInteractParticle(x, y) result(res)
        real(8) :: x, y
        logical :: res
        res = x <= x_max1 .and. x >= x_min1 .and. y <= upperInteractionBound1 .and. y >= bottomInteractionBound1
    end function

    function IsCalculateParticle(x, y) result(res)
        real(8) :: x, y
        logical :: res
        res = x <= x_max1 .and. x >= x_min1 .and. y >= bottomBound1
        !взять ответственность у MpiRank=0 за частицы на upperBound1
        if(res) then
            if(MpiRank == 0) then
                res = y <= upperBound1
            else
                res = y < upperBound1
            endif
        endif
    end function
    
    function IsTransferToUpperParticle(y) result(res)
        real(8), intent(in) :: y
        logical :: res
        if(MpiRank == 0) then
            res = .false.
        else
            res = y >= upperBound1-hLimit1
        endif
    end function

    function IsTransferToBottomParticle(y) result(res)
        real(8), intent(in) :: y
        logical :: res
        if(MpiRank == MpiSize-1) then
            res = .false.
        else
            res = y <= bottomBound1 + hLimit1
        endif
    end function
        
    subroutine MoveParticles()
        integer(4) :: i
        integer(4) :: calculateCursor
        !передвигаем частички
        do calculateCursor=1, calculateParticles(0)
            i = calculateParticles(calculateCursor)
            call ParticleMover(point(i))
        enddo
    end subroutine
    
    subroutine InjectParticles()
        implicit none
        integer(4) :: calculateCursor
        
        calculateCursor = calculateParticles(0)
        call ParticlesCreator(dt1, AddPointCallBack)
        calculateParticles(0) = calculateCursor

    contains
    subroutine AddPointCallBack(pointToAdd)
        type(PointType), intent(in) :: pointToAdd
        integer(4) :: acquiredIndex
        if(IsCalculateParticle(pointToAdd % r(1), pointToAdd % r(2))) then
            acquiredIndex = freePointPool % Acquire()
            point(acquiredIndex) = pointToAdd

            calculateCursor = calculateCursor + 1
            calculateParticles(calculateCursor) = acquiredIndex
        endif
    end subroutine
        
    end subroutine
    
    subroutine UpdatePointParameters()
        abstract interface
        function IsTransferToFunctionPointer(y) result(res)
            real(8), intent(in) :: y
            logical :: res
        end function
        end interface
        integer(4) :: i, dataExchangeIndex, pointIndex
        integer(4) :: calculateCursor, calculateNextCursor, interactNextCursor, interactCursor
        real(8) :: x, y
        type(DataExchangeType), pointer :: targetExchangeData
        procedure (IsTransferToFunctionPointer), pointer :: IsTransferToFunction => null ()
        integer(4) :: upperRecvCount, bottomRecvCount
        integer(4) :: upperDelimiterIndex, bottomDelimiterIndex

        !передвигаем частички в ноде
        call MoveParticles()
        
        !добавляем частички в ноду (! Меняет текущий calculateParticles !)
        call InjectParticles()

        !освободим чужие частицы
        do i = 1, foreignPointsIndexes % Cursor
            pointIndex = foreignPointsIndexes % Instance(i)
            call ReleasePoint(pointIndex)
        enddo
        foreignPointsIndexes % Cursor = 0
        
        !проставить 0 частиц для новой итерации
        interactParticlesNext(0) = 0
        calculateParticlesNext(0) = 0
        interactNextCursor = 0
        calculateNextCursor = 0
        
        !обновить координаты полос
        !call UpdateBorders()
        
        !сбросим данные для пересылаемых частиц в начальные значения
        targetExchangeData => upperNodeData
        do dataExchangeIndex = 1, 2
            targetExchangeData % sendIndexes % Cursor = 0
            targetExchangeData % recvIndexes % Cursor = 0
            targetExchangeData % recvPointIndexes % Cursor = 0
            targetExchangeData % sendPointIndexes % Cursor = 0
            targetExchangeData => bottomNodeData
        enddo

        !подготовим индексы для передачи (и приема), если приема частиц этой ноды
        do calculateCursor=1, calculateParticles(0)
            pointIndex = calculateParticles(calculateCursor)
            
            x = point(pointIndex) % r(1)
            y = point(pointIndex) % r(2)

            if(IsActualParticle(x, y)) then
                !узнаем куда надо отсылать...или не надо?
                if(IsTransferToUpperParticle(y)) then
                    targetExchangeData => upperNodeData
                else if(IsTransferToBottomParticle(y)) then
                    targetExchangeData => bottomNodeData
                else
                    nullify(targetExchangeData)
                endif
                
                if(associated(targetExchangeData)) then !надо отсылать
                    call targetExchangeData % sendPointIndexes % SafeAddItem(pointIndex) !добавим в частицы на отсылку
                    if(IsInteractParticle(x, y)) then !мы все еще взаимодействуем с ее данными
                        if(IsCalculateParticle(x, y)) then !и она еще наша - значит нужно от нас отсылать данные другим
                            call targetExchangeData % sendIndexes % SafeAddItem(pointIndex)
                        else !и она уже не наша - значит нужно нам получать данные от других
                            call targetExchangeData % recvIndexes % SafeAddItem(pointIndex)
                        endif
                    endif
                endif
                
                !запишем наши или бывшие у нас частицы в массивы индексов частиц
                if(IsInteractParticle(x, y)) then
                    interactNextCursor = interactNextCursor + 1
                    interactParticlesNext(interactNextCursor) = pointIndex
                    
                    if(IsCalculateParticle(x, y)) then !частица в области ответственности ноды то добавим в массив ответственности
                        calculateNextCursor = calculateNextCursor + 1
                        calculateParticlesNext(calculateNextCursor) = pointIndex
                    else
                        call foreignPointsIndexes % SafeAddItem(pointIndex) !теперь это чужая частица
                    endif
                endif
                
            else
                call ReleasePoint(pointIndex)
            endif
        enddo
             
        !запомним где заканчиваются точки от нас и начинаются точки от чужой ноды
        upperDelimiterIndex = upperNodeData % recvIndexes % Cursor
        bottomDelimiterIndex = bottomNodeData % recvIndexes % Cursor
        
        !передадим и получим количество частиц информацию о которых мы хотим обменятся (без тех информацию о которых надо будет передавать но мы о них еще не знаем)
        call MpiExchangeTwoIntegers( &
            upperSendValue = upperNodeData % sendPointIndexes % Cursor, &
            bottomSendValue = bottomNodeData % sendPointIndexes % Cursor, &
            upperRecvValue = upperRecvCount, &
            bottomRecvValue = bottomRecvCount)

        !выделим индексы под новые частицы
        do i = 1, upperRecvCount
            pointIndex = freePointPool % Acquire()
            call upperNodeData % recvPointIndexes % SafeAddItem(pointIndex)
        enddo
        do i = 1, bottomRecvCount
            pointIndex = freePointPool % Acquire()
            call bottomNodeData % recvPointIndexes % SafeAddItem(pointIndex)
        enddo
        
        call MpiExchangeTwoIndexedSubArrays( &
            targetArray = point, &
            upperSendIndexes = upperNodeData % sendPointIndexes, &
            bottomSendIndexes = bottomNodeData % sendPointIndexes, &
            upperRecvIndexes = upperNodeData % recvPointIndexes, &
            bottomRecvIndexes = bottomNodeData % recvPointIndexes, &
            MpiType = MPI_TYPE_TransferPoint)

#ifdef DEBUG        
        call MpiAssert(upperRecvCount == upperNodeData % recvPointIndexes % Cursor)        
        call MpiAssert(bottomRecvCount == bottomNodeData % recvPointIndexes % Cursor)
#endif

        !мы передали частицы и те, которые нам недосягаемы должны быть освобождены
        targetExchangeData => upperNodeData
        do dataExchangeIndex = 1, 2
            do i = 1, targetExchangeData % sendPointIndexes % Cursor
                pointIndex = targetExchangeData % sendPointIndexes % Instance(i)
                x = point(pointIndex) % r(1)
                y = point(pointIndex) % r(2)
                if(.not. IsInteractParticle(x, y)) then
                    call ReleasePoint(pointIndex)
                endif
            enddo
            targetExchangeData => bottomNodeData
        enddo
        
        !разберем, что нам пришло сверху и снизу
        targetExchangeData => upperNodeData
        IsTransferToFunction => IsTransferToUpperParticle
        do dataExchangeIndex = 1, 2
            !добавить прибывшие частицы
            do i=1, targetExchangeData % recvPointIndexes % Cursor
                !индекс присланной частицы
                pointIndex = targetExchangeData % recvPointIndexes % Instance(i)

                x = point(pointIndex) % r(1)
                y = point(pointIndex) % r(2)

                if(.not. IsInteractParticle(x, y)) then !прилетела частица которая не нужна ноде для взаимодействия??? плохо
                    call MpiAbortWithMessage("Particle is invalid")
                endif

#ifdef CALCULATE_PARTICLE_INTERACTION_COUNT
#ifdef DEBUG
                point(pointIndex) % interactionCount = -1
#endif
#endif
                interactNextCursor = interactNextCursor + 1
                interactParticlesNext(interactNextCursor) = pointIndex

                if(IsCalculateParticle(x, y)) then !частица в зоне ответственности ноды
                    calculateNextCursor = calculateNextCursor + 1
                    calculateParticlesNext(calculateNextCursor) = pointIndex
                    
                    if(IsTransferToFunction(y)) then !мы за нее отвечаем и должны передать о ней информацию
                        call targetExchangeData % sendIndexes % SafeAddItem(pointIndex)
                    endif
                else !частица в зоне ответственности другой ноды но мы с ней взаимодействуем
                    call foreignPointsIndexes % SafeAddItem(pointIndex)
                    call targetExchangeData % recvIndexes % SafeAddItem(pointIndex)
                endif
            enddo
           
            targetExchangeData => bottomNodeData
            IsTransferToFunction => IsTransferToBottomParticle
        enddo

        !обновим количество частиц
        calculateParticlesNext(0) = calculateNextCursor
        interactParticlesNext(0) = interactNextCursor
        call SwapActualParticlesArrays()

        !индексы тех частиц которые ушли (но которые нам все еще нужны) и тех которые пришли (и теперь нам нужны)
        !находятся в разных порядках между нодами
        !эти функции переставляют индексы в правильный порядок, синхронизируя порядок в списках индексов между нодами
        call ShiftIndexes(upperNodeData, upperDelimiterIndex)
        call ShiftIndexes(bottomNodeData, bottomDelimiterIndex)
        
        !теперь все частицы находятся на своих местах в своих нодах но для них не пересчитаны Rho и P и T
#ifdef USE_CLUSTER_MODE
        call UpdateClusterGrid()
#endif
        !после обновления кластерной сетки можно считать параметры по области
        call UpdateRho()
        
        !передадим и примем rho
        call MpiExchangeTwoIndexedSubArrays( &
            targetArray = point, &
            upperSendIndexes = upperNodeData % sendIndexes, &
            bottomSendIndexes = bottomNodeData % sendIndexes, &
            upperRecvIndexes = upperNodeData % recvIndexes, &
            bottomRecvIndexes = bottomNodeData % recvIndexes, &
            MpiType = MPI_TYPE_TransferPointRho)
        
        !обновим P и T_gas, T_rad
        do interactCursor=1, interactParticles(0)
            i = interactParticles(interactCursor)
            point(i) % p = point(i) % u * point(i) % rho * (5d0 / 3d0 - 1d0)
            point(i) % T_gas = point(i) % p * mu1 / (R_gas_const1 * point(i) % rho)
            point(i) % T_rad =  (point(i)%ksi * point(i)%rho * c_const1 / 4d0 / sigma_const1)**(0.25d0)
        enddo

    end subroutine
    
    subroutine ReleasePoint(pointIndex)
    integer(4) :: pointIndex
#ifdef DEBUG
        point(pointIndex) % v = -1d0
        point(pointIndex) % r = -1d0
        point(pointIndex) % dr = -1d0
        point(pointIndex) % u = -1d0
        point(pointIndex) % ksi = -1d0
        point(pointIndex) % p = -1d0
        point(pointIndex) % rho = -1d0
#ifdef CALCULATE_PARTICLE_INTERACTION_CUNT
        point(pointIndex) % interactionCount = -1
#endif                
#endif
        call freePointPool  %  Release(pointIndex)
    end subroutine
    
    subroutine ShiftIndexes(dataExchange, delimiterIndex)
       type(DataExchangeType) :: dataExchange
       integer(4), intent(in) :: delimiterIndex
       integer(4) :: i, j, shiftIndex, start
       
       !надо сначала перечислить ребят от Delimiter + 1 до Cursor а потом от 1 до Delimiter

        if(delimiterIndex == 0) then
            return
        endif

        if(delimiterIndex == dataExchange % recvIndexes % Cursor) then
            return
        endif
        
        start = 0
        do i = delimiterIndex + 1, dataExchange % recvIndexes % Cursor
            start = start + 1
            shiftIndex = dataExchange % recvIndexes % Instance(i)
            do j = i, start + 1, -1
                dataExchange % recvIndexes % Instance(j) = dataExchange % recvIndexes % Instance(j-1)
            enddo
            dataExchange % recvIndexes % Instance(start) = shiftIndex
        enddo
   end subroutine
   

    function GetViscosity(r_ab, v_ab, rho_a, rho_b, p_a, p_b) result(viscAb)
        real(8) :: viscAb
        real(8), parameter :: alpha = 1d0
        real(8), parameter :: beta = 2d0
        real(8), parameter :: eta2 = 0.01d0 * h1**2
        real(8) :: rv_ab, c_ab, mu_ab, rho_ab
        real(8) :: C_a, C_b
        real(8), intent(in) :: rho_a, rho_b, p_a, p_b
        real(8), dimension(2), intent(in) :: r_ab, v_ab
        
        rv_ab = dot_product(r_ab, v_ab)
        
        if(rv_ab <= 0d0) then
       
            C_a = GetSpeedOfSound(rho_a, p_a)
            C_b = GetSpeedOfSound(rho_b, p_b)

            c_ab = (C_a + C_b) / 2d0
            mu_ab = h1 * rv_ab / (dot_product(r_ab, r_ab) + eta2)
            rho_ab = (rho_a + rho_b) / 2d0
            
            viscAb = mu_ab * (beta * mu_ab - alpha * c_ab) / rho_ab

        else
            viscAb = 0d0
        endif
    end function
    
    !скорость звука
    function GetSpeedOfSound(rho, p) result(c)
        real(8), intent(in) :: rho, p
        real(8) :: T, C
        
        T = p * mu1 / (R_gas_const1 * rho)
        c = dsqrt(Poisson_const * k_B_const1 * T / m_H_const1)
    end function
    
end module
