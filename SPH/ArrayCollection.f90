module MODULE_NAME
#ifdef COLLECTION_USE_MODULE
    use :: COLLECTION_USE_MODULE
#endif
implicit none

    type  COLLECTION_TYPE_NAME
        integer(4) :: defaultAllocationSize
        integer(4) :: expandMultiplyer
        GENERIC_TYPE, allocatable, dimension(:) :: Instance
        integer(4) :: Cursor
    contains
        procedure :: SafeAddItem => SafeAddItem
        procedure :: Dispose => Dispose
        procedure :: Expand => Expand
        procedure :: EnsureNextItem => EnsureNextItem
    end type

    private :: SafeAddItem, Dispose, Expand, EnsureNextItem
    
    contains

    subroutine Dispose(this)
        class(COLLECTION_TYPE_NAME), intent(inout) :: this
        if(allocated(this % Instance)) then
            deallocate(this % Instance)
        endif
    end subroutine
    
    subroutine Expand(this, requredSize)
        class(COLLECTION_TYPE_NAME), intent(inout) :: this
        integer(4), intent(in) :: requredSize
        integer(4) :: arraySize, newArraySize
        logical :: wasAllocated

        if(requredSize == 0) then
            return
        endif
        
        wasAllocated = allocated(this % Instance)
        arraySize = uBound(this % Instance, dim=1)
        
        if(requredSize > arraySize) then
            
            newArraySize = this % defaultAllocationSize * this % expandMultiplyer
            if(newArraySize < requredSize) then
                newArraySize = requredSize * this % expandMultiplyer
            endif
            
            if(wasAllocated) then
                deallocate(this % Instance)
            endif
            
            allocate(this % Instance(newArraySize))
        endif
        
    end subroutine
    
    subroutine EnsureNextItem(this)
        class(COLLECTION_TYPE_NAME), intent(inout) :: this
        integer(4) :: requredSize
        GENERIC_TYPE, allocatable, dimension(:) :: bufferArrayLink
        integer(4) :: arraySize, newArraySize
        logical :: wasAllocated
        
        requredSize = this % Cursor + 1
        wasAllocated = allocated(this % Instance)
        arraySize = uBound(this % Instance, dim=1)
        
        if(requredSize > arraySize) then
            
            newArraySize = this % defaultAllocationSize * this % expandMultiplyer
            if(newArraySize < requredSize) then
                newArraySize = requredSize * this % expandMultiplyer
            endif
            
            if(wasAllocated) then
                allocate(bufferArrayLink(newArraySize))
                bufferArrayLink(1:arraySize) = this % Instance
                deallocate(this % Instance)
                call move_alloc(bufferArrayLink, this % Instance)
            else
                allocate(this % Instance(newArraySize))
            endif
        endif
    end subroutine
    
    subroutine SafeAddItem(this, item)
        class(COLLECTION_TYPE_NAME), intent(inout) :: this
        GENERIC_TYPE, intent(in) :: item
        
        call this % EnsureNextItem()

        this % Cursor = this % Cursor + 1
        this % Instance(this % Cursor) = item
    end subroutine
end module
