module MODULE_NAME
implicit none
#ifdef COLLECTION_USE_MODULE
    use :: COLLECTION_USE_MODULE
#endif
    type COLLECTION_TYPE_NAME
        private
        integer(4) :: PoolTop
        GENERIC_TYPE, dimension(:), allocatable :: Instance
    contains
        procedure :: Acquire => Acquire
        procedure :: Release => Release
        procedure :: FillPoolElements => FillPoolElements
        procedure :: Dispose => Dispose
    end type

    private :: Acquire, Release, FillPoolElements, Dispose
    
    contains
    
    subroutine Dispose(this)
        class(COLLECTION_TYPE_NAME), intent(inout) :: this
        if(allocated(this % Instance)) then
            deallocate(this % Instance)
        endif
    end subroutine
    
    subroutine FillPoolElements(this, Filler, ItemCount)
        class(COLLECTION_TYPE_NAME), intent(inout) :: this
        integer(4) :: i
        integer(4), intent(in) :: ItemCount
        interface
        function Filler() result(item)
            GENERIC_TYPE :: item
        end function
        end interface

        if(allocated(this % Instance)) then
            deallocate(this % Instance)
        endif
        
        allocate(this % Instance(ItemCount))
        
        do i = 1, ItemCount
            this % Instance(ItemCount - i + 1) = Filler()
        enddo
        
        this % PoolTop = ItemCount
    end subroutine

    function Acquire(this) result(res)
        class(COLLECTION_TYPE_NAME), intent(inout) :: this
        GENERIC_TYPE :: res
        
        if(this % PoolTop == 0) then
            write(*,*) "Particle pool is empty"
            stop
        endif
        
        res = this % Instance(this % PoolTop)
        this % PoolTop = this % PoolTop - 1
    end function
    
    subroutine Release(this, index)
        class(COLLECTION_TYPE_NAME), intent(inout) :: this
        GENERIC_TYPE, intent(in) :: index
    
       if(this % PoolTop == ubound(this % Instance, 1)) then
            write(*,*) "Particle pool is full"
            stop
       endif
        
       this % PoolTop = this % PoolTop + 1
       this % Instance(this % PoolTop) = index
    
    end subroutine
end module
