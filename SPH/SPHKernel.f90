module SPHKernel
    use :: Parameters
    use :: Helpers
    implicit none
    
    real(8), private :: h1
    real(8), private :: w_factor
    real(8), private :: gradW_factor
    real(8), private :: laplaceW_factor
    
    contains
    
    subroutine SPHKernelInitialize(init_h1)
    real(8), intent(in) :: init_h1
        h1 = init_h1
        w_factor = 75d0 / 153472d0 / pi_const / h1**2
        gradW_factor = 75d0 / 153472d0 / pi_const / h1**3
        laplaceW_factor = 75d0 / 153472d0 / pi_const / h1**2    
    end subroutine
    

    !function GetW(r_ab) result(W_ab)
    !    real(8), intent(in), dimension(2) :: r_ab
    !    real(8) :: W_ab
    !    real(8) :: x, dist_ab
    !
    !    dist_ab = VecNorm(r_ab)  
    !    x = dist_ab / h1
    !
    !    W_ab = 0d0
    !    if(x <= 1d0) then
    !        W_ab = 1d0 - 3d0 / 2d0 * x**2 + 3d0 / 4d0 * x**3
    !    endif
    !    if(1d0 < x .and. x <= 2d0) then
    !        W_ab = 1d0 / 4d0 * (2d0 - x)**3
    !    endif
    !
    !    W_ab = W_ab * 10d0 / 7d0 / pi_const / h1**2
    !
    !end function

    !function GetGradW(r_ab) result(gradW_ab)
    !    real(8), dimension(2) :: r_ab
    !    real(8), dimension(2) :: gradW_ab
    !    real(8) :: x, dist_ab, factor
    !
    !    if(r_ab(1) == 0d0 .and. r_ab(2) == 0d0) then
    !        gradW_ab = 0d0
    !        return
    !    endif
    !    
    !    dist_ab = VecNorm(r_ab)
    !    
    !    if(dist_ab < 1d-9) then
    !        r_ab = r_ab / dist_ab * 1d-9
    !        dist_ab = 1d-9
    !    endif
    !    
    !    x = dist_ab / h1
    !    
    !    if(x <= 2d0 / 3d0) then
    !        gradW_ab =  4d0
    !    else if (2d0 / 3d0 < x .and. x <= 1d0) then
    !        gradW_ab =  3d0 * x * (4d0-3d0 * x) 
    !    else if (1d0 < x .and. x <= 2d0) then
    !        gradW_ab = 3d0 * (2d0-x)**2
    !    else
    !        gradW_ab =  0d0
    !        return
    !    endif
    !    
    !    factor = -10d0 / 7d0 / pi_const / h1**3 / 4d0
    !    gradW_ab(1) = gradW_ab(1) * r_ab(1) * factor / dist_ab
    !    gradW_ab(2) = gradW_ab(2) * r_ab(2) * factor / dist_ab
    !end function

    function GetW(r_ab) result(W_ab)
        real(8), intent(in), dimension(2) :: r_ab
        real(8) :: W_ab
        real(8) :: x, dist_ab
        
        dist_ab = VecNorm(r_ab)
        x = dist_ab / h1

        if(x <= 2d0 / 5d0) then
            W_ab = (10d0 - 5d0 * x)**4 - 5d0 * (6d0 - 5d0 * x)**4 + 10d0 * (2d0 - 5d0 * x)**4
        else if(2d0 / 5d0 < x .and. x <= 6d0 / 5d0) then
            W_ab = (10d0 - 5d0 * x)**4 - 5d0 * (6d0 - 5d0 * x)**4
        else if(6d0 / 5d0 < x .and. x <= 2d0) then
            W_ab = (10d0 - 5d0 * x)**4
        else
            W_ab = 0d0
            return
        endif
    
        W_ab = W_ab * w_factor
    
    end function
    
    subroutine GetGradW(r_ab, gradW, gradW_devided_By_R)
        real(8), dimension(2) :: r_ab
        real(8), dimension(2), intent(out), optional :: gradW
        real(8), intent(out), optional :: gradW_devided_By_R
        real(8) :: x, dist_ab, normGradW_ab

        if(r_ab(1) == 0d0 .and. r_ab(2) == 0d0) then
            if(present(gradW)) then
                gradW = 0d0
            endif
            if(present(gradW_devided_By_R)) then
                gradW_devided_By_R = 0d0
            endif
            return
        endif
        
        dist_ab = VecNorm(r_ab)
        
        if(dist_ab < 1d-9) then
            r_ab = r_ab / dist_ab * 1d-9
            dist_ab = 1d-9
        endif
        
        x = dist_ab / h1
        
        if(x >= 2d0) then
            if(present(gradW)) then
                gradW = 0d0
            endif
            if(present(gradW_devided_By_R)) then
                gradW_devided_By_R = 0d0
            endif
            return
        endif

        if(x <= 0.0001d0) then
    	    normGradW_ab = 0.05999999924d0
        else if(0.0001d0 < x .and. x <= 2d0 / 5d0) then
            normGradW_ab = (10d0 - 5d0 * x)**3 - 5d0 * (6d0 - 5d0 * x)**3 + 10d0 * (2d0 - 5d0 * x)**3
        else if(2d0 / 5d0 < x .and. x <= 6d0 / 5d0) then
            normGradW_ab = (10d0 - 5d0 * x)**3 - 5d0 * (6d0 - 5d0 * x)**3
        else
            normGradW_ab = (10d0 - 5d0 * x)**3
        endif
        
        normGradW_ab = - 20d0 * normGradW_ab
        normGradW_ab = normGradW_ab * gradW_factor

        if(present(gradW)) then
            gradW = normGradW_ab * r_ab / dist_ab
        endif
        if(present(gradW_devided_By_R)) then
            gradW_devided_By_R = normGradW_ab / (x * h1)
        endif
        
    end subroutine
    
    function GetLaplaceW(r_ab) result(laplaceW_ab)
        real(8), dimension(2) :: r_ab
        real(8) :: laplaceW_ab, firstDevirge, secondDevirge
        real(8) :: x, dist_ab

        dist_ab = VecNorm(r_ab)
        
        if(dist_ab < 1d-9) then
            r_ab = r_ab / dist_ab * 1d-9
            dist_ab = 1d-9
        endif
        
        x = dist_ab / h1
        
        if(x >= 2d0) then
            laplaceW_ab = 0d0
            return
        endif
        
        if(x <= 0.0001d0) then
    	    secondDevirge = 0d0
        else if(0.0001d0 < x .and. x <= 2d0 / 5d0) then
            secondDevirge = 300d0 * (10d0 - 5d0 * x)**2 - 1500d0 * (6d0 - 5d0 * x)**2 + 3000d0 * (2d0 - 5d0 * x)**2
        else if(2d0 / 5d0 < x .and. x <= 6d0 / 5d0) then
            secondDevirge = 300d0 * (10d0 - 5d0 * x)**2 - 1500d0 * (6d0 - 5d0 * x)**2
        else
            secondDevirge = 300d0 * (10d0 - 5d0 * x)**2
        endif
        
        if(x <= 0.0001d0) then
    	    firstDevirge = 0.05999999924
        else if(0.0001d0 < x .and. x <= 2d0 / 5d0) then
            firstDevirge = (10d0 - 5d0 * x)**3 - 5d0 * (6d0 - 5d0 * x)**3 + 10d0 * (2d0 - 5d0 * x)**3
        else if(2d0 / 5d0 < x .and. x <= 6d0 / 5d0) then
            firstDevirge = (10d0 - 5d0 * x)**3 - 5d0 * (6d0 - 5d0 * x)**3
        else
            firstDevirge = (10d0 - 5d0 * x)**3
        endif
        
        firstDevirge = -20d0 * firstDevirge
        
        firstDevirge = firstDevirge / h1 / dist_ab
        
        secondDevirge = secondDevirge / h1**2
        
        laplaceW_ab = (firstDevirge + secondDevirge) * laplaceW_factor
    end function
end module
    